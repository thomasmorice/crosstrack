module.exports = {
  mode: "jit",
  purge: [
    "src/pages/**/*.{js,ts,jsx,tsx,scss}",
    "src/components/**/*.{js,ts,jsx,tsx,scss}",
    "src/styles/**/*.{css,scss}",
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        crosstrack: {
          weightlifting: "var(--weightlifting-color)",
          gymnastic: "var(--gymnastic-color)",
          cardio: "var(--cardio-color)",
          gray: {
            100: "#807f7f",
            200: "#606060",
            300: "#34363C",
            400: "#31363f",
          },
        },
      },
      screens: {
        "light-mode": { raw: "(prefers-color-scheme: light)" },
      },
    },
    fontFamily: {
      sans: ["Lato", "sans-serif"],
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require("daisyui")],
  daisyui: {
    themes: ["dark"],
  },
};
