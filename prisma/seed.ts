import { exercises } from "./exercises";
import { workouts, creatorId } from "./workouts";
// import { users } from "./users";
import {
  Prisma,
  PrismaClient,
  WorkoutElement,
  WorkoutExercise,
} from "@prisma/client";

const prisma = new PrismaClient();

async function main() {
  // await Promise.all(
  //   users.map(async (user) => {
  //     const userExists = await prisma.user.findUnique({
  //       where: {
  //         email: user.email as string,
  //       },
  //     });
  //     if (!userExists) {
  //       await prisma.user.create({
  //         data: user,
  //       });
  //       console.log(`user ${user.name} created`);
  //     } else {
  //       console.log(`user ${user.name} already exists`);
  //     }
  //   })
  // );

  // await prisma.workoutExercise.deleteMany();
  // await prisma.workoutExercise.deleteMany();

  await Promise.all(
    exercises.map(async (exercise) => {
      await prisma.exercise.upsert({
        where: {
          name: exercise.name,
        },
        create: exercise as Prisma.ExerciseCreateInput,
        update: exercise as Prisma.ExerciseCreateInput,
      });
      console.log(`exercise ${exercise.name} created or updated`);
    })
  );

  await prisma.workout.deleteMany({
    where: {
      creatorId,
    },
  });

  const exercisesFromDb = await prisma.exercise.findMany();

  await Promise.all(
    workouts.map(async (workout) => {
      const elementsAsCreate: Prisma.WorkoutElementCreateInput[] =
        workout.workoutElements.map(
          (
            element: WorkoutElement & {
              workoutExercises: WorkoutExercise[];
            }
          ) => {
            return {
              order: element.order,
              annotation: element.annotation || "",
              transitionAnnotation: element.transitionAnnotation,
              workoutExercises: {
                create: element.workoutExercises.map((workoutExercise: any) => {
                  const exerciseId = exercisesFromDb.find(
                    (exercise) =>
                      exercise.name === workoutExercise.exercise?.name
                  )?.id;
                  return {
                    reps: workoutExercise.reps || "",
                    annotation: workoutExercise.annotation || "",
                    exerciseId: exerciseId,
                    order: workoutExercise.order,
                  };
                }),
              },
            };
          }
        );

      await prisma.workout.create({
        // update: {
        //   creatorId: workout.creatorId,
        //   description: workout.description || "",
        //   type: workout.type || "",
        //   timecap: workout.timecap,
        //   workoutElements: {
        //     create: elementsAsCreate,
        //   },
        // },
        // where: {
        //   name: workout.name,
        // },
        data: {
          creatorId: workout.creatorId || creatorId,
          name: workout.name || "",
          description: workout.description || "",
          type: workout.type || "",
          timecap: workout.timecap,
          workoutElements: {
            create: elementsAsCreate,
          },
        },
      });
      console.log(`workout ${workout.name} added`);
    })
  );

  process.exit(0);
}

main()
  .catch((e) => {
    console.log("error", e);
    process.exit(1);
  })
  .finally(() => {
    prisma.$disconnect;
  });
