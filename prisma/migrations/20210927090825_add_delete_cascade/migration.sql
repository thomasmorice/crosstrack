-- DropForeignKey
ALTER TABLE "WorkoutElement" DROP CONSTRAINT "WorkoutElement_workoutId_fkey";

-- DropForeignKey
ALTER TABLE "WorkoutExercise" DROP CONSTRAINT "WorkoutExercise_workoutElementId_fkey";

-- AddForeignKey
ALTER TABLE "WorkoutElement" ADD CONSTRAINT "WorkoutElement_workoutId_fkey" FOREIGN KEY ("workoutId") REFERENCES "Workout"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WorkoutExercise" ADD CONSTRAINT "WorkoutExercise_workoutElementId_fkey" FOREIGN KEY ("workoutElementId") REFERENCES "WorkoutElement"("id") ON DELETE CASCADE ON UPDATE CASCADE;
