/*
  Warnings:

  - Made the column `workoutId` on table `WorkoutElement` required. This step will fail if there are existing NULL values in that column.
  - Made the column `workoutElementId` on table `WorkoutExercise` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE "WorkoutElement" DROP CONSTRAINT "WorkoutElement_workoutId_fkey";

-- DropForeignKey
ALTER TABLE "WorkoutExercise" DROP CONSTRAINT "WorkoutExercise_workoutElementId_fkey";

-- AlterTable
ALTER TABLE "WorkoutElement" ALTER COLUMN "workoutId" SET NOT NULL;

-- AlterTable
ALTER TABLE "WorkoutExercise" ALTER COLUMN "workoutElementId" SET NOT NULL;

-- AddForeignKey
ALTER TABLE "WorkoutElement" ADD CONSTRAINT "WorkoutElement_workoutId_fkey" FOREIGN KEY ("workoutId") REFERENCES "Workout"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WorkoutExercise" ADD CONSTRAINT "WorkoutExercise_workoutElementId_fkey" FOREIGN KEY ("workoutElementId") REFERENCES "WorkoutElement"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
