/*
  Warnings:

  - Added the required column `order` to the `WorkoutElement` table without a default value. This is not possible if the table is not empty.
  - Added the required column `order` to the `WorkoutExercise` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "WorkoutElement" ADD COLUMN     "order" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "WorkoutExercise" ADD COLUMN     "order" INTEGER NOT NULL;
