-- CreateTable
CREATE TABLE "Workout" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "timecap" INTEGER,
    "description" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "creatorId" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "Workout_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "WorkoutElement" (
    "id" SERIAL NOT NULL,
    "annotation" TEXT NOT NULL,
    "transitionAnnotation" TEXT,
    "workoutId" INTEGER,

    CONSTRAINT "WorkoutElement_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "WorkoutExercise" (
    "id" SERIAL NOT NULL,
    "reps" TEXT NOT NULL,
    "annotation" TEXT,
    "exerciseId" INTEGER NOT NULL,
    "workoutElementId" INTEGER,

    CONSTRAINT "WorkoutExercise_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "WorkoutElement" ADD CONSTRAINT "WorkoutElement_workoutId_fkey" FOREIGN KEY ("workoutId") REFERENCES "Workout"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WorkoutExercise" ADD CONSTRAINT "WorkoutExercise_exerciseId_fkey" FOREIGN KEY ("exerciseId") REFERENCES "exercises"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WorkoutExercise" ADD CONSTRAINT "WorkoutExercise_workoutElementId_fkey" FOREIGN KEY ("workoutElementId") REFERENCES "WorkoutElement"("id") ON DELETE SET NULL ON UPDATE CASCADE;
