import { exercises } from "./exercises";

export const creatorId = "cku2jc8xf0008kko8qo9xevb7";

export const workouts: any[] = [
  {
    creatorId,
    name: "lucky-moccasin",
    type: "Yellow",
    timecap: 18,
    description: "",
    workoutElements: [
      {
        order: 0,
        annotation: "",
        workoutExercises: [
          {
            reps: "21",
            exercise:
              exercises.find((exercise) => exercise.name === "box-jump over") ||
              null,
            order: 0,
            annotation: "",
          },
          {
            reps: "15",
            order: 1,
            exercise:
              exercises.find((exercise) => exercise.name === "push press") ||
              null,
            annotation: "RX 45/30KG",
          },
          {
            reps: "9",
            order: 2,
            exercise:
              exercises.find((exercise) => exercise.name === "toes-to-bar") ||
              null,
          },
        ],
      },
    ],
  },
  {
    creatorId,
    name: "killer-amrap",
    type: "Black (skills)",
    timecap: 10,
    description: "",
    workoutElements: [
      {
        order: 0,
        annotation: "",
        workoutExercises: [
          {
            order: 0,
            reps: "15",
            exercise:
              exercises.find((exercise) => exercise.name === "power snatch") ||
              null,
            annotation: "60/35 kg or 60 - 70% of 1 RM",
          },
          {
            order: 1,
            reps: "50",
            exercise:
              exercises.find((exercise) => exercise.name === "double-under") ||
              null,
            annotation: "Scaled to 100 single unders ",
          },
          {
            order: 2,
            reps: "15",
            exercise:
              exercises.find((exercise) => exercise.name === "deadlift") ||
              null,
            annotation: "120/85 kg or 60 - 70% of 1 RM",
          },
          {
            order: 3,
            reps: "50",
            exercise:
              exercises.find((exercise) => exercise.name === "double-under") ||
              null,
            annotation: "Scaled to 100 single unders ",
          },
          {
            order: 4,
            reps: "15",
            exercise:
              exercises.find(
                (exercise) => exercise.name === "front rack lunge"
              ) || null,
            annotation: "backward lunges - 60/35 kg",
          },
          {
            order: 5,
            reps: "50",
            exercise:
              exercises.find((exercise) => exercise.name === "double-under") ||
              null,
            annotation: "Scaled to 100 single unders ",
          },
        ],
      },
    ],
  },
];
