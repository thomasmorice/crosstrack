import { Modality } from "@prisma/client";

interface Exercise extends Partial<import("@prisma/client").Exercise> {
  modalityId: import("@prisma/client").Modality | null;
  equipmentId: import("components/Equipment/Equipment.constants").EquipmentType["id"];
}

export const exercises: Exercise[] = [
  {
    name: "deadlift",
    modalityId: Modality.WEIGHTLIFTING,
    hasRepMax: true,
    protected: true,
    equipmentId: "barbell",
  },
  {
    name: "power snatch",
    modalityId: Modality.WEIGHTLIFTING,
    hasRepMax: true,
    protected: true,
    equipmentId: "barbell",
  },
  {
    name: "push press",
    modalityId: Modality.WEIGHTLIFTING,
    hasRepMax: true,
    protected: true,
    equipmentId: "barbell",
  },
  {
    name: "sumo deadlift high pull",
    modalityId: Modality.WEIGHTLIFTING,
    hasRepMax: false,
    protected: true,
    equipmentId: "barbell",
  },
  {
    name: "burpee",
    modalityId: Modality.GYMNASTIC,
    hasRepMax: false,
    protected: true,
    equipmentId: "no-equipment",
  },
  {
    name: "double-under",
    modalityId: Modality.CARDIO,
    hasRepMax: false,
    equipmentId: "jump-rope",
    protected: true,
  },
  {
    name: "single-under",
    modalityId: Modality.CARDIO,
    hasRepMax: false,
    equipmentId: "jump-rope",
    protected: true,
  },
  {
    name: "pronated pull-up",
    modalityId: Modality.GYMNASTIC,
    hasRepMax: false,
    protected: true,
    equipmentId: "pullup-bar",
  },
  {
    name: "ring push-up",
    modalityId: Modality.GYMNASTIC,
    hasRepMax: false,
    protected: true,
    equipmentId: "unknown",
  },
  {
    name: "backsquat",
    modalityId: Modality.WEIGHTLIFTING,
    hasRepMax: true,
    protected: true,
    equipmentId: "no-equipment",
  },
  {
    name: "rowing",
    modalityId: Modality.CARDIO,
    hasRepMax: false,
    protected: true,
    equipmentId: "unknown",
  },
  {
    name: "box-jump over",
    modalityId: Modality.CARDIO,
    hasRepMax: false,
    protected: true,
    equipmentId: "box",
  },
  {
    name: "toes-to-bar",
    modalityId: Modality.GYMNASTIC,
    hasRepMax: false,
    protected: true,
    equipmentId: "pullup-bar",
  },
  {
    name: "front rack lunge",
    modalityId: Modality.WEIGHTLIFTING,
    hasRepMax: false,
    protected: true,
    equipmentId: "barbell",
  },
];
