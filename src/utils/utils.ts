export const getCssVariableInJs = (colorName: string): string => {
  const color = getComputedStyle(document.documentElement).getPropertyValue(
    colorName
  );
  return color;
};
