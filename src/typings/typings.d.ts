declare module "uuid";
declare module "react-mini-chart";
declare module "lodash-es";

declare type Exercise = Omit<
  import("@prisma/client").Exercise,
  "id" | "creatorId" | "createdAt"
> & {
  id: number | undefined;
  modalityId: import("@prisma/client").Modality | null;
  equipmentId: import("components/Equipment/Equipment.constants").EquipmentType["id"];
};

// interface Exercise extends Partial<import("@prisma/client").Exercise> {
//   modalityId: import("@prisma/client").Modality | null;
//   equipmentId: import("components/Equipment/Equipment.constants").EquipmentType["id"];
// }

// declare type Workout = Omit<
//   import("@prisma/client").Workout,
//   "id" | "creatorId" | "createdAt" | "timecap"
// > & {
//   id: number | null;
//   timecap: number | null;
//   workoutElements: WorkoutElement[];
// };

interface Workout extends Partial<import("@prisma/client").Workout> {
  workoutElements: WorkoutElement[];
}

interface WorkoutElement
  extends Partial<import("@prisma/client").WorkoutElement> {
  workoutExercises: WorkoutExercise[];
}

interface WorkoutExercise
  extends Partial<import("@prisma/client").WorkoutExercise> {
  exercise: Exercise | null;
}

type FormType =
  | "bordered"
  | "ghost"
  | "primary"
  | "secondary"
  | "accent"
  | "info"
  | "success"
  | "warning"
  | "error";

type FormInput<T> = {
  id: string;
  name?: string;
  className?: string;
  placeholder: string;
  disabled?: boolean;
  value?: string;
  error?: string;
  onChange: (e: ChangeEvent<T>) => void;
  type?:
    | "bordered"
    | "ghost"
    | "primary"
    | "secondary"
    | "accent"
    | "info"
    | "success"
    | "warning"
    | "error";
};

type Tab = {
  id: number;
  label: string;
};

type RequestError = {
  error: Error;
  message: string;
};
