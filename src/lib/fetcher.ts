export default async function fetcher(url: string) {
  const res = await fetch(url);

  // If the status code is not in the range 200-299,
  // we still try to parse and throw it.
  if (!res.ok) {
    const error: RequestError = {
      error: new Error("An error occurred while fetching the data."),
      message: "The api url seems invalid",
    };

    // Attach extra info to the error object.
    throw error;
  }

  return res.json();
}
