type LabelProps = {
  children: string;
  isRequired?: boolean;
};

export default function Label(props: LabelProps) {
  const { children, isRequired } = props;
  return (
    <label className="label">
      <span className="label-text">
        {children}
        {isRequired && <span className="text-error text-lg"> * </span>}
      </span>
    </label>
  );
}
