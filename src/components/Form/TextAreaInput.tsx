import { AnimatePresence, motion } from "framer-motion";
import { useEffect, useRef } from "react";
import ContentEditable from "react-contenteditable";

type TextAreaProps = {
  label?: string;
  inputProps: FormInput<ElementContentEditable>;
};

export default function TextAreaInput({ inputProps, label }: TextAreaProps) {
  const contentEditableRef = useRef<HTMLDivElement>(null);
  const contentEditableListener: EventListener = (e: any) => {
    e.preventDefault();
    const text = e.clipboardData.getData("text/plain");
    document.execCommand("insertHTML", false, text);
  };

  useEffect(() => {
    if (contentEditableRef && contentEditableRef.current) {
      contentEditableRef.current.addEventListener(
        "paste",
        contentEditableListener
      );
      return () => {
        contentEditableRef?.current?.removeEventListener(
          "paste",
          contentEditableListener
        );
      };
    }
  }, [contentEditableRef]);

  const animationVariants = {
    initial: {
      opacity: 0,
      height: 0,
    },
    animate: {
      opacity: 1,
      height: "auto",
    },
    exit: {
      opacity: 0,
      height: 0,
    },
  };
  return (
    <div>
      <AnimatePresence>
        {!inputProps.disabled && (
          <motion.label
            key="textarea-label"
            initial="initial"
            animate="animate"
            exit="exit"
            transition={{ duration: 0.2 }}
            variants={animationVariants}
            className="label"
          >
            <span className="label-text text-crosstrack-gray-100">{label}</span>
          </motion.label>
        )}
      </AnimatePresence>

      <div ref={contentEditableRef}>
        <ContentEditable
          id={inputProps.id}
          className={`
              block h-auto w-full text-gray-400 placeholder-gray-500
              ${
                inputProps.disabled
                  ? "p-0 bg-transparent border-transparent text-crosstrack-gray-200 "
                  : `textarea textarea-${inputProps.type}`
              }
              ${inputProps.className} 
            `}
          html={inputProps.value || ""}
          disabled={inputProps.disabled}
          onChange={inputProps.onChange}
        ></ContentEditable>
      </div>
    </div>
  );
}
