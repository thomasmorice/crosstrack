import { ChangeEvent } from "react";

type ToggleProps = {
  id: string;
  label: string;
  checked: boolean;
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
  type?: FormType;
};

export default function Toggle(props: ToggleProps) {
  const { id, label, checked, onChange, type } = props;
  return (
    <>
      <label className="cursor-pointer label justify-start gap-x-4">
        <input
          id={id}
          type="checkbox"
          checked={checked}
          className={`toggle toggle-${type}`}
          onChange={onChange}
        />
        <span className="label-text">{label}</span>
      </label>
    </>
  );
}
