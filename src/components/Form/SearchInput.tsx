import TextInput from "./TextInput";
import { XCircleIcon, PlusIcon } from "@heroicons/react/solid";
import LoadingIcon from "components/LoadingIcon/LoadingIcon";

type SearchInputProps<T> = {
  searchInputProps: FormInput<HTMLInputElement>;
  valueLocked: boolean;
  unlockValue: () => void;
  onSelect: (item: T) => void;
  selectItems: {
    items: T[];
    key: keyof T;
    label: keyof T;
  };
  handleAddNewItem: () => void;
  isSearching?: boolean;
  showAddNewItem?: boolean;
};

export default function SearchInput<T>({
  searchInputProps,
  valueLocked,
  unlockValue,
  selectItems,
  onSelect,
  handleAddNewItem,
  isSearching = false,
  showAddNewItem = false,
}: SearchInputProps<T>) {
  return (
    <div className="relative">
      <div className="flex items-center">
        <input
          id={searchInputProps.id}
          name={searchInputProps.name}
          placeholder={searchInputProps.placeholder}
          value={searchInputProps.value || ""}
          onChange={searchInputProps.onChange}
          disabled={valueLocked}
          className={`
              transition-all input ${searchInputProps.className} text-gray-300 placeholder-gray-500 
              disabled:text-gray-300 disabled:py-0 disabled:pl-3 disabled:pr-10 disabled:border disabled:bg-base-100
              input-${searchInputProps.type}
            `}
        />
        {valueLocked && (
          <div onClick={unlockValue} className="-ml-8 cursor-pointer">
            <XCircleIcon className="w-6" />
          </div>
        )}
        {isSearching && (
          <div onClick={unlockValue} className="-ml-8">
            <LoadingIcon className="-ml-1 mr-3 h-5 w-5 text-white" />
          </div>
        )}
      </div>
      {selectItems && (
        <div className="absolute flex flex-col mt-2 bg-base-100 rounded-xl max-h-52 w-full overflow-y-scroll">
          {selectItems.items.map((item, index) => {
            return (
              <div
                key={index}
                className={`
                  flex py-3 px-4 cursor-pointer hover:bg-gray-600
                  ${index > 0 ? "" : ""}
                `}
                onClick={() => onSelect(item)}
              >
                {item[selectItems.label]}
              </div>
            );
          })}
          {showAddNewItem && (
            <div
              key={"add-new-item"}
              className={`
                  flex items-center py-3 px-4 cursor-pointer bg-crosstrack-gray-200 hover:bg-gray-600
              `}
              onClick={handleAddNewItem}
            >
              <PlusIcon className="h-4 mr-2" /> Add new item
            </div>
          )}
        </div>
      )}
    </div>
  );
}
