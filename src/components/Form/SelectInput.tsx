import { ChangeEvent } from "react";

type SelectInputProps<T> = {
  id: string;
  options: {
    array: T[];
    id: keyof T;
    label: keyof T;
  };
  value?: string;
  defaultValue?: string;
  onChange: (e: ChangeEvent<HTMLSelectElement>) => void;
  placeholder: string;
  type?: FormType;
  error?: string;
};

export default function SelectInput<T>(props: SelectInputProps<T>) {
  const {
    id,
    placeholder,
    options,
    onChange,
    value,
    defaultValue,
    type,
    error,
  } = props;
  return (
    <>
      <select
        id={id}
        className={`select select-${type} w-full max-w-xs text-base-content`}
        // defaultValue={defaultValue ? defaultValue : ""}
        value={value}
        onChange={onChange}
      >
        <option disabled={true} value={value ? defaultValue : ""}>
          {placeholder}
        </option>
        {options.array.map((item) => (
          <option
            key={String(item[options.id])}
            value={String(item[options.id])}
          >
            {" "}
            {item[options.label]}
          </option>
        ))}
      </select>
      {error && <div className="text-error text-sm mt-1"> {error} </div>}
    </>
  );
}
