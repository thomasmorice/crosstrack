import { AnimatePresence, motion } from "framer-motion";

export type TextInputProps<HTMLInputElement> = {
  inputProps: FormInput<HTMLInputElement>;
  type?: "text" | "number";
  width?: string;
  maxLength?: number;
  prepend?: string;
};

export default function TextInput(props: TextInputProps<HTMLInputElement>) {
  // const animationVariants = {
  //   initial: {
  //     opacity: 0,
  //     height: 0,
  //   },
  //   animate: {
  //     opacity: 1,
  //     height: "auto",
  //   },
  //   exit: {
  //     opacity: 0,
  //     height: 0,
  //   },
  // };

  const { inputProps, type, maxLength, width, prepend } = props;
  return (
    <>
      <div className={`flex`}>
        {prepend && (
          <button
            className={`rounded-r-none btn btn-disabled btn-${inputProps.type} normal-case`}
          >
            {prepend}
          </button>
        )}
        <div className={`w-${width}`}>
          {!inputProps.disabled ? (
            <div>
              <input
                id={inputProps.id}
                maxLength={maxLength}
                name={inputProps.name}
                type={type ? type : "text"}
                placeholder={inputProps.placeholder}
                value={inputProps.value || ""}
                onChange={inputProps.onChange}
                className={`
              transition-all input  ${
                inputProps.className
              } max-h-full text-gray-300 placeholder-gray-500 disabled:text-crosstrack-gray-200
              input-${inputProps.type} ${prepend ? "rounded-l-none" : ""}
            `}
              />
            </div>
          ) : (
            <div className={`h-full flex items-center`}>
              {" "}
              {inputProps.value}{" "}
            </div>
          )}
        </div>
      </div>
      {inputProps.error && (
        <div className="text-error text-sm mt-2 -mb-1">
          {" "}
          {inputProps.error}{" "}
        </div>
      )}
    </>
  );
}
