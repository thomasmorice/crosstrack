import TextAreaInput from "components/Form/TextAreaInput";
import WorkoutExercise from "../WorkoutExercise/WorkoutExercise";

type WorkoutElementProps = {
  element: WorkoutElement;
  onChange: (element: WorkoutElement) => void;
  isEditing?: boolean;
};

export default function WorkoutElement({
  element,
  isEditing = false,
  onChange,
}: WorkoutElementProps) {
  const handleAddWorkoutExercise = (workoutExercise: WorkoutExercise) => {
    let tempWorkoutExercises = [...element.workoutExercises];
    tempWorkoutExercises.push(workoutExercise);
    onChange({
      ...element,
      workoutExercises: tempWorkoutExercises,
    });
  };

  const handleChangeWorkoutExercise = (
    index: number,
    workoutExercise: WorkoutExercise
  ) => {
    let tempWorkoutExercises = [...element.workoutExercises];
    tempWorkoutExercises[index] = workoutExercise;
    onChange({
      ...element,
      workoutExercises: tempWorkoutExercises,
    });
  };

  const handleDelete = (index: number) => {
    let tempWorkoutExercises = [...element.workoutExercises];
    tempWorkoutExercises.splice(index, 1);
    onChange({
      ...element,
      workoutExercises: tempWorkoutExercises,
    });
  };

  return (
    <div className="flex flex-col gap-y-3 text-sm leading-5">
      {element?.workoutExercises.map((workoutExercise, index) => {
        return (
          <WorkoutExercise
            key={index}
            isEditing={isEditing}
            workoutExercise={workoutExercise}
            handleDelete={() => handleDelete(index)}
            handleChange={(workoutExercise) =>
              handleChangeWorkoutExercise(index, workoutExercise)
            }
          />
        );
      })}
      {isEditing && (
        <WorkoutExercise
          emptyWorkoutExercise
          handleAdd={handleAddWorkoutExercise}
        />
      )}
      <TextAreaInput
        label="Element description"
        inputProps={{
          id: "element-comment",
          placeholder: "add comments...",
          type: "ghost",
          className: "leading-5",
          onChange: (e) => {
            onChange({
              ...element,
              annotation: e.target.value,
            });
          },
          disabled: !isEditing,
          value: element?.annotation,
        }}
      ></TextAreaInput>
    </div>
  );
}
