import { useState, useEffect } from "react";
import styles from "./WorkoutCard.module.scss";
import Avatar from "components/User/Avatar";
import { useSession } from "next-auth/client";
import MiniChart from "react-mini-chart";

import {
  PlusCircleIcon,
  ClockIcon,
  CalendarIcon,
  FireIcon,
  ClipboardListIcon,
  BackspaceIcon,
} from "@heroicons/react/solid";
import TextInput from "components/Form/TextInput";
import WorkoutElement from "../WorkoutElement/WorkoutElement";
import TextAreaInput from "components/Form/TextAreaInput";
import { AnimatePresence, motion } from "framer-motion";
import WorkoutCardActions from "./WorkoutCardActions";
import Tooltip from "components/Tooltip/Tooltip";
import { elementService } from "services/element.service";
import WorkoutModalityShare from "components/Modality/WorkoutModalityShares/WorkoutModalityShares";
import { FormikErrors, useFormikContext } from "formik";

const animationVariants = {
  initial: {
    opacity: 0,
    height: 0,
    marginTop: 0,
  },
  animate: {
    opacity: 1,
    height: "auto",
    marginTop: "0.8rem", // hack for transition height
  },
  exit: {
    opacity: 0,
    height: 0,
    margin: 0,
  },
};

type WorkoutCardProps = {
  workout: Workout;
  isNewWorkout?: boolean;
  isBeingEdited?: boolean;
  isExpanded?: boolean;
  isSaving?: boolean;
  isDirty?: boolean;
  handleExpand: () => void;
  handleCancelCreateNewWorkout?: () => void;
  handleDelete: () => void;
  onChange: (id: keyof Workout, value: any) => void;
  handleEditMode: () => void;
  handleClose: () => void;
  formErrors: FormikErrors<Workout>;
};

export default function WorkoutCard({
  workout,
  isNewWorkout = false,
  isBeingEdited = false,
  isSaving = false,
  isExpanded = false,
  isDirty = false,
  handleExpand,
  handleEditMode,
  handleDelete,
  onChange,
  handleClose,
  formErrors,
}: WorkoutCardProps) {
  const [session, loading] = useSession();

  const { submitForm } = useFormikContext();

  const handleAddNewElement = () => {
    let tempElements = [...workout.workoutElements];
    tempElements.push(elementService.getEmptyElement());
    onChange("workoutElements", tempElements);
  };

  const handleRemoveElement = (index: number) => {
    let tempElements = [...workout.workoutElements];
    tempElements.splice(index, 1);
    onChange("workoutElements", tempElements);
  };

  const handleUpdateElement = (index: number, element: WorkoutElement) => {
    let elements = [...workout.workoutElements];
    elements[index] = element;
    onChange("workoutElements", elements);
  };

  return (
    <>
      <div
        className={`
        ${styles.container} 
        ${isBeingEdited && styles["is-editing"]}
        ${isNewWorkout && styles["is-new-workout"]}
      `}
      >
        <div className={styles.background}>
          <WorkoutCardActions
            isSaving={isSaving}
            isExpanded={isExpanded}
            isBeingEdited={isBeingEdited}
            isWorkoutDirty={isDirty}
            handleDelete={handleDelete}
            handleSave={() => submitForm()}
            handleEdit={handleEditMode}
            handleExpand={handleExpand}
            handleClose={handleClose}
          />

          <div className={styles["title-and-avatar"]}>
            {!loading && <Avatar img={session?.user.image} />}
            <TextInput
              inputProps={{
                id: "name",
                className: isBeingEdited ? "input-sm" : "",
                placeholder: "Name",
                value: workout.name,
                disabled: !isBeingEdited,
                onChange: (e) => onChange("name", e.target.value),
              }}
            />
          </div>

          <div className={styles.informations}>
            <div
              suppressHydrationWarning={true}
              className={styles["modality-chart"]}
            >
              <WorkoutModalityShare workout={workout} />
            </div>

            <div className={styles.items}>
              <div className={styles.item}>
                <ClockIcon />{" "}
                <TextInput
                  maxLength={4}
                  inputProps={{
                    id: "timecap",
                    className: "input-xs w-12 mr-2",
                    placeholder: "21",
                    value: workout.timecap ? workout.timecap + "" : "",
                    disabled: !isBeingEdited,
                    onChange: (e) => {
                      onChange("timecap", parseInt(e.target.value) || null);
                    },
                  }}
                />
                mn
              </div>
              <div className="text-error">{formErrors.timecap}</div>
              <div className={styles.item}>
                <CalendarIcon />{" "}
                {workout.createdAt &&
                  new Date(workout.createdAt).toLocaleDateString("fr-FR")}
              </div>
              <div className={styles.item}>
                <FireIcon />
                <TextInput
                  inputProps={{
                    id: "type",
                    className: "input-xs w-30 mr-5",
                    placeholder: "type of workout",
                    value: workout.type,
                    disabled: !isBeingEdited,
                    onChange: (e) => {
                      onChange("type", e.target.value);
                    },
                  }}
                />
              </div>
              <div className="text-error">{formErrors.type}</div>
            </div>
          </div>

          {/* General workout comments */}
          <div className={styles.comments}>
            <TextAreaInput
              label="Workout description"
              inputProps={{
                id: "description",
                placeholder: "Workout comments",
                type: "ghost",
                className: "leading-5",
                onChange: (e) => {
                  onChange("description", e.target.value);
                },
                disabled: !isBeingEdited,
                value: workout.description,
              }}
            ></TextAreaInput>
          </div>

          {/* Last workout date */}
          <div
            suppressHydrationWarning={true}
            className={styles["last-result"]}
          >
            <div className="flex items-center gap-x-1">
              <ClipboardListIcon />
              Last result - 21/05/2021
            </div>
            {process.browser && (
              <MiniChart
                labelFontSize={15}
                activePointColor={"#606060"}
                strokeColor={"#606060"}
                strokeWidth={2}
                width={30}
                height={24}
                dataSet={[0, 20, 30, 50, 42, 55]}
              />
            )}
          </div>

          {/* First workout element */}
          <div className="mt-[-0.8rem]">
            <AnimatePresence>
              {(isExpanded || isBeingEdited) && (
                <motion.div
                  key="workout-elements"
                  className="flex flex-col gap-y-4"
                  transition={{
                    type: "spring",
                    duration: 0.5,
                  }}
                  initial="initial"
                  animate="animate"
                  exit="exit"
                  variants={animationVariants}
                >
                  <WorkoutElement
                    onChange={(element) => {
                      handleUpdateElement(0, element);
                    }}
                    isEditing={isBeingEdited}
                    element={workout.workoutElements[0]}
                  />
                </motion.div>
              )}
            </AnimatePresence>
          </div>
        </div>

        {/* Additional elements */}
        {((isExpanded && workout.workoutElements.length > 0) ||
          isBeingEdited) &&
          workout.workoutElements.map(
            (element, index) =>
              index > 0 && (
                <div key={`element-${index}`}>
                  <div className={`${styles["new-element"]}`}>
                    <div className={styles.line}></div>
                    <div className={`${styles.element} w-full`}>
                      <TextInput
                        inputProps={{
                          id: `annotation-transition-${index}`,
                          placeholder: "2 minutes break",
                          className: "input-sm w-72 text-center",
                          onChange: (e) => {
                            handleUpdateElement(index, {
                              ...[...workout.workoutElements][index],
                              transitionAnnotation: e.target.value,
                            });
                          },
                          disabled: !isBeingEdited,
                          value: element.transitionAnnotation || undefined,
                        }}
                      />
                    </div>
                    <div className={styles.line}></div>
                  </div>
                  <div key={index} className={`${styles.background}`}>
                    <div className="flex w-full justify-between items-center text-crosstrack-gray-100 -mt-4">
                      <>
                        {`Element ${index + 1}`}
                        {isBeingEdited && (
                          <Tooltip text="Remove element">
                            <BackspaceIcon
                              onClick={() => handleRemoveElement(index)}
                              className="w-7 ml-3 cursor-pointer"
                            />
                          </Tooltip>
                        )}
                      </>
                    </div>
                    <WorkoutElement
                      key={index}
                      onChange={(element) => {
                        handleUpdateElement(index, element);
                      }}
                      isEditing={isBeingEdited}
                      element={element}
                    />
                  </div>
                </div>
              )
          )}

        {/* Add new element section */}
        {isBeingEdited && (
          <div onClick={handleAddNewElement} className={styles["new-element"]}>
            <div className={styles.line}></div>
            <div className={`${styles.element} ${styles.new}`}>
              <Tooltip text="Add new workout element">
                <PlusCircleIcon width={22} />
              </Tooltip>
            </div>
          </div>
        )}
      </div>
    </>
  );
}
