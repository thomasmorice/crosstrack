import {
  PencilAltIcon,
  XCircleIcon,
  SaveIcon,
  ArrowsExpandIcon,
  TrashIcon,
  MenuAlt2Icon,
} from "@heroicons/react/solid";
import LoadingIcon from "components/LoadingIcon/LoadingIcon";
import Modal from "components/Modal/Modal";
import Tooltip from "components/Tooltip/Tooltip";
import { useState } from "react";
import styles from "./WorkoutCard.module.scss";

type WorkoutCardActionsProps = {
  isBeingEdited?: boolean;
  isExpanded?: boolean;
  isWorkoutDirty?: boolean;
  isSaving?: boolean;
  handleDelete: () => void;
  handleEdit: () => void;
  handleExpand: () => void;
  handleSave: () => void;
  handleClose: () => void;
};

export default function WorkoutCardActions({
  isSaving = false,
  isBeingEdited = false,
  isExpanded = false,
  isWorkoutDirty = false,
  handleDelete,
  handleEdit,
  handleExpand,
  handleSave,
  handleClose,
}: WorkoutCardActionsProps) {
  const [showDeleteModal, set_showDeleteModal] = useState(false);
  return (
    <>
      <div className={styles.menu}>
        {/* {!isBeingEdited && !isSaving && (
          <Tooltip text="Delete workout">
            <TrashIcon onClick={() => set_showDeleteModal(true)} />
          </Tooltip>
        )}
        {!isBeingEdited && !isSaving && (
          <Tooltip text="Edit workout">
            <PencilAltIcon onClick={handleEdit} />
          </Tooltip>
        )} */}
        {!isBeingEdited && !isSaving && (
          <div className="dropdown dropdown-end">
            <div tabIndex={0} className="h-[28px]">
              <MenuAlt2Icon />
            </div>
            <ul
              tabIndex={0}
              className="p-2 shadow menu dropdown-content bg-base-100 text-base-content rounded-box w-52"
            >
              <li>
                <a onClick={() => console.log("add results")}>Add results</a>
              </li>
              <li>
                <a onClick={handleEdit}>Edit workout</a>
              </li>
              <li>
                <a
                  onClick={() => set_showDeleteModal(true)}
                  className="bg-error"
                >
                  Delete workout
                </a>
              </li>
            </ul>
          </div>
        )}

        {!isExpanded && !isBeingEdited && !isSaving && (
          <Tooltip text="Show workout element">
            <ArrowsExpandIcon
              className={styles.stroked}
              onClick={handleExpand}
            />
          </Tooltip>
        )}
        {isWorkoutDirty && !isSaving && (
          <Tooltip text="Save changes">
            <SaveIcon onClick={handleSave} />
          </Tooltip>
        )}
        {(isExpanded || isBeingEdited) && !isSaving && (
          <Tooltip text="Close">
            <XCircleIcon onClick={handleClose} />
          </Tooltip>
        )}
        {isSaving && (
          <Tooltip text="Saving">
            <LoadingIcon />
          </Tooltip>
        )}
      </div>

      {showDeleteModal && (
        <Modal
          actions={{
            accept: {
              label: "Yes delete",
              onClick: () => {
                handleDelete();
                set_showDeleteModal(false);
              },
            },
            close: {
              label: "Cancel",
              onClick: () => set_showDeleteModal(false),
            },
          }}
        >
          <>
            <h3 className="text-base-content text-lg mb-4">
              {" "}
              Delete a workout{" "}
            </h3>
            <p className="text-base-content">
              {" "}
              Are your sure you want to delete this workout?{" "}
            </p>
          </>
        </Modal>
      )}
    </>
  );
}
