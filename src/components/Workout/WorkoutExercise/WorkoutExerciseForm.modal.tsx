import Label from "components/Form/Label";
import SearchInput from "components/Form/SearchInput";
import TextInput from "components/Form/TextInput";
import Modal from "components/Modal/Modal";
import { useContext, useEffect, useState } from "react";
import { exerciseService } from "services/exercise.service";
import { workoutExerciseService } from "services/workoutExercise.service";
import { ExerciseFormContext } from "components/Exercise/ExerciseForm/ExerciseFormContext";

type WorkoutExerciseFormProps = {
  workoutExercise?: WorkoutExercise;
  onChange: (changes: WorkoutExercise) => void;
  onClose: () => void;
};

export default function WorkoutExerciseForm({
  workoutExercise,
  onChange,
  onClose,
}: WorkoutExerciseFormProps) {
  const [searchExerciseTerm, set_searchExerciseTerm] = useState<string>("");
  const [updatedWorkoutExercise, set_updatedWorkoutExercise] =
    useState<WorkoutExercise>(
      workoutExercise || workoutExerciseService.getEmptyWorkoutExercise()
    );

  const { createExercise, savedExercise, resetSavedExercise } =
    useContext(ExerciseFormContext);

  const [formError, set_formError] = useState<string>("");
  const [isSearching, set_isSearching] = useState(false);

  const [exerciseSearchResults, set_exerciseSearchResults] = useState<
    Exercise[]
  >([]);

  const handleSaveChanges = () => {
    if (updatedWorkoutExercise.reps === "") {
      set_formError("Please add repetitions");
    } else if (!updatedWorkoutExercise.exercise?.id) {
      set_formError("Please select an exercise");
    } else {
      // const newWorkoutExercise: WorkoutExercise = { ...updatedWorkoutExercise };
      onChange({
        ...updatedWorkoutExercise,
      });
      onClose();
    }
  };

  useEffect(() => {
    if (savedExercise?.id) {
      set_updatedWorkoutExercise({
        ...updatedWorkoutExercise,
        exercise: savedExercise,
      });
      resetSavedExercise();
      set_searchExerciseTerm("");
    }
  }, [savedExercise]);

  useEffect(() => {
    async function searchExercises() {
      set_exerciseSearchResults([]);
      if (searchExerciseTerm.length >= 2) {
        set_isSearching(true);
        set_exerciseSearchResults(
          await exerciseService.searchExercises(searchExerciseTerm)
        );
        set_isSearching(false);
      }
    }
    searchExercises();
  }, [searchExerciseTerm]);

  return (
    <>
      <Modal>
        <div className="text-base-content">
          <h3 className="text-xl mb-3"> Edit the workout exercise </h3>
          <Label> Repetitions </Label>
          <TextInput
            inputProps={{
              id: "reps",
              className: "w-full",
              placeholder: "12",
              value: updatedWorkoutExercise?.reps,
              onChange: (e) => {
                set_updatedWorkoutExercise({
                  ...updatedWorkoutExercise,
                  reps: e.target.value,
                });
              },
              type: "primary",
            }}
            width="full"
          />
          <Label> Exercise </Label>
          <SearchInput<Exercise>
            searchInputProps={{
              id: "exercise",
              placeholder: "Powerclean",
              value: updatedWorkoutExercise.exercise?.id
                ? updatedWorkoutExercise.exercise?.name
                : searchExerciseTerm,
              onChange: (e) => {
                set_searchExerciseTerm(e.target.value);
              },
              type: "primary",
              className: "w-full",
            }}
            valueLocked={!!updatedWorkoutExercise.exercise?.id}
            unlockValue={() => {
              set_updatedWorkoutExercise({
                ...updatedWorkoutExercise,
                exercise: null,
              });
            }}
            selectItems={{
              items: exerciseSearchResults,
              key: "id",
              label: "name",
            }}
            onSelect={(exercise: Exercise) => {
              set_updatedWorkoutExercise({
                ...updatedWorkoutExercise,
                exercise,
              });
              set_searchExerciseTerm("");
            }}
            isSearching={isSearching}
            showAddNewItem={searchExerciseTerm.length > 1}
            handleAddNewItem={createExercise}
          />
          <Label> Annotation </Label>
          <TextInput
            inputProps={{
              id: "annotation",
              className: "w-full",
              placeholder: "Alternating left and right",
              value: updatedWorkoutExercise.annotation || undefined,
              onChange: (e) => {
                set_updatedWorkoutExercise({
                  ...updatedWorkoutExercise,
                  annotation: e.target.value,
                });
              },
              type: "primary",
            }}
            width="full"
          />

          {formError !== "" && <p className="text-error mt-4"> {formError} </p>}
          <div className="modal-action">
            <button
              type="button"
              className="btn btn-primary"
              onClick={handleSaveChanges}
            >
              Update
            </button>
            <button type="button" className="btn btn-ghost" onClick={onClose}>
              Cancel
            </button>
          </div>
        </div>
      </Modal>
    </>
  );
}
