import styles from "./WorkoutExercise.module.scss";
import { PlusCircleIcon } from "@heroicons/react/solid";
import EquipmentIcon from "components/Equipment/EquipmentIcon";
import { MenuAlt4Icon } from "@heroicons/react/solid";
import Menu from "components/Menu/menu";
import WorkoutExerciseForm from "./WorkoutExerciseForm.modal";
import { useState } from "react";

type WorkoutExerciseProps = {
  isEditing?: boolean;
  emptyWorkoutExercise?: boolean;
  workoutExercise?: WorkoutExercise;
  handleAdd?: (workoutExercise: WorkoutExercise) => void;
  handleChange?: (workoutExercise: WorkoutExercise) => void;
  handleDelete?: () => void;
};

export default function WorkoutExercise({
  emptyWorkoutExercise = false,
  isEditing = false,
  workoutExercise,
  handleAdd,
  handleChange,
  handleDelete,
}: WorkoutExerciseProps) {
  const modalityId = workoutExercise?.exercise?.modalityId?.toLowerCase() || "";
  const [showEditWorkoutExerciseModal, set_showEditWorkoutExerciseModal] =
    useState<boolean>(false);

  const handleEditWorkoutExercise = () => {
    set_showEditWorkoutExerciseModal(true);
  };

  const handleDeleteWorkoutExercise = () => {
    handleDelete && handleDelete();
  };

  return (
    <>
      <div className={styles.container}>
        {emptyWorkoutExercise ? (
          <div
            onClick={() => set_showEditWorkoutExerciseModal(true)}
            className={styles["add-new-workout-exercise"]}
          >
            <PlusCircleIcon className="w-5" /> Add a new exercise
          </div>
        ) : (
          <div className={styles["exercise-container"]}>
            <div className="flex">
              {/* Modality indicator */}
              <div
                className={`${styles["modality-indicator"]} ${styles[modalityId]}`}
              ></div>

              <div className={styles["workout-exercise"]}>
                <div className={styles.title}>
                  {workoutExercise?.reps}{" "}
                  <span className="capitalize">
                    {workoutExercise?.exercise?.name}
                  </span>
                </div>
                <div className={styles.description}>
                  {workoutExercise?.annotation}
                </div>
              </div>
            </div>

            <div className={styles["menu-and-equipment"]}>
              <div>
                {isEditing && (
                  <Menu
                    MenuIcon={<MenuAlt4Icon className="w-7 cursor-pointer" />}
                    options={[
                      {
                        onClick: () => handleEditWorkoutExercise(),
                        label: "Edit",
                        // Icon: <PencilIcon width="20" className="mr-2" />,
                      },
                      {
                        className: "bg-error",
                        onClick: () => handleDeleteWorkoutExercise(),
                        label: "Delete",
                        // Icon: <TrashIcon width="20" className="mr-2" />,
                      },
                    ]}
                  />
                )}
              </div>

              <div className={styles.equipment}>
                <EquipmentIcon
                  modalityId={workoutExercise?.exercise?.modalityId || null}
                  equipmentId={
                    workoutExercise?.exercise?.equipmentId || "unknown"
                  }
                />
              </div>
            </div>
          </div>
        )}
      </div>
      {showEditWorkoutExerciseModal && (
        <WorkoutExerciseForm
          workoutExercise={workoutExercise}
          onChange={(changes: WorkoutExercise) => {
            handleChange
              ? handleChange({
                  ...workoutExercise,
                  ...changes,
                })
              : handleAdd &&
                handleAdd({
                  ...workoutExercise,
                  ...changes,
                });
          }}
          onClose={() => set_showEditWorkoutExerciseModal(false)}
        />
      )}
    </>
  );
}
