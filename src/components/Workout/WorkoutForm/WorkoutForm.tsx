import { ToastContext } from "components/Toast/ToastContext";
import { useContext, useEffect, useState } from "react";
import { workoutService } from "services/workout.service";
import * as Yup from "yup";
import { Formik } from "formik";
import WorkoutCard from "components/Workout/WorkoutCard/WorkoutCard";
import Modal from "components/Modal/Modal";
import { motion } from "framer-motion";

type WorkoutFormProps = {
  workout: Workout;
  handleCancelCreateNewWorkout?: () => void;
};

const WorkoutValidationSchema = Yup.object().shape({
  name: Yup.string().required("A name for the workout is required"),
  timecap: Yup.number()
    .typeError("Timecap must be a number")
    .required("The timecap is required"),
  type: Yup.string().required("The type of workout is required"),
});

// The workout form is parent of a workout card
export default function WorkoutForm({
  workout,
  handleCancelCreateNewWorkout,
}: WorkoutFormProps) {
  const { addToast } = useContext(ToastContext);
  const [isWorkoutDirty, set_isWorkoutDirty] = useState<boolean>(false);
  const [isExpanded, set_isExpanded] = useState<boolean>(false);
  const [isBeingEdited, set_isBeingEdited] = useState<boolean>(!workout.id);
  const [showCancelWorkoutModal, set_showCancelWorkoutModal] =
    useState<boolean>(false);

  const handleCloseForm = (forceClose?: boolean) => {
    if (forceClose || !isWorkoutDirty) {
      set_isWorkoutDirty(false);
      set_isExpanded(false);
      set_isBeingEdited(false);
      handleCancelCreateNewWorkout && handleCancelCreateNewWorkout();
    } else {
      set_showCancelWorkoutModal(true);
    }
  };

  return (
    <>
      <Formik<Workout>
        enableReinitialize
        initialValues={workout}
        validateOnChange={false}
        validationSchema={WorkoutValidationSchema}
        onSubmit={async (workout, actions) => {
          actions.setSubmitting(true);
          const response = !workout.id
            ? await workoutService.addWorkout(workout)
            : await workoutService.editWorkout(workout);
          actions.setSubmitting(false);
          handleCloseForm(true);
          actions.resetForm();
          workoutService.refetchUserWorkouts();
        }}
      >
        {({
          // handleChange,
          handleSubmit,
          values,
          resetForm,
          setFieldValue,
          errors,
          setErrors,
          setSubmitting,
          isSubmitting,
        }) => {
          const handleChange = (id: string, value: any) => {
            setErrors({});
            let newValues = {
              ...values,
              [id]: value,
            };
            set_isWorkoutDirty(
              !workoutService.areWorkoutsEquals(workout, newValues)
            );
            setFieldValue(id, value);
          };

          const handleDelete = async (workout: Workout) => {
            setSubmitting(true);
            await workoutService.deleteWorkout(workout);
            workoutService.refetchUserWorkouts();
            setSubmitting(false);
          };

          return (
            <form className="form-control" onSubmit={handleSubmit}>
              <WorkoutCard
                workout={values}
                isNewWorkout={!workout.id}
                handleCancelCreateNewWorkout={handleCancelCreateNewWorkout}
                onChange={(id: string, value: any) => {
                  handleChange(id, value);
                }}
                isSaving={isSubmitting}
                isDirty={isWorkoutDirty}
                handleDelete={() => handleDelete(workout)}
                handleEditMode={() => set_isBeingEdited(true)}
                isBeingEdited={isBeingEdited}
                handleClose={() => handleCloseForm()}
                isExpanded={isExpanded}
                handleExpand={() => set_isExpanded(true)}
                formErrors={errors}
              />

              {/* Modal and blurry background  */}
              {showCancelWorkoutModal && (
                <Modal
                  actions={{
                    accept: {
                      label: "Cancel changes",
                      onClick: () => {
                        resetForm();
                        set_showCancelWorkoutModal(false);
                        handleCloseForm(true);
                      },
                    },
                    close: {
                      label: "Keep editing",
                      onClick: () => set_showCancelWorkoutModal(false),
                    },
                  }}
                >
                  <>
                    <h3 className="text-xl mb-3 font-extrabold">
                      Unsaved changes
                    </h3>
                    <p>
                      Your current changes aren&apos;t saved, do you want to
                      cancel your changes?
                    </p>
                  </>
                </Modal>
              )}
            </form>
          );
        }}
      </Formik>
      {isBeingEdited && (
        <motion.div
          style={{
            backdropFilter: "blur(4px)",
            backgroundColor: "rgba(0,0,0,0.5)",
          }}
          className="fixed top-0 left-0 z-20 h-full w-full"
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{
            opacity: 0,
            transition: {
              duration: 0.5,
            },
          }}
        ></motion.div>
      )}
    </>
  );
}
