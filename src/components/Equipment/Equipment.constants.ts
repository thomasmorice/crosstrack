import AssaultBikeIcon from "./images/assault-bike.svg";
import BarbellIcon from "./images/barbell.svg";
import BoxIcon from "./images/box.svg";
import DipsBarIcon from "./images/dips-bar.svg";
import DumbellIcon from "./images/dumbell.svg";
import JumpRopeIcon from "./images/jump-rope.svg";
import KettlebellIcon from "./images/kettlebell.svg";
import MedecineBallIcon from "./images/medecine-ball.svg";
import NoEquipmentIcon from "./images/no-equipment.svg";
import PullupBarIcon from "./images/pullup-bar.svg";
import UnknownIcon from "./images/unknown.svg";
import WallIcon from "./images/wall.svg";
import RowerIcon from "./images/rower.svg";

export type EquipmentType = {
  id:
    | "unknown"
    | "assault-bike"
    | "barbell"
    | "jump-rope"
    | "box"
    | "dips-bar"
    | "rower"
    | "dumbell"
    | "kettlebell"
    | "medecine-ball"
    | "no-equipment"
    | "pullup-bar"
    | "wall";
  label: string;
  icon: any;
};

export const Equipments: EquipmentType[] = [
  {
    id: "unknown",
    label: "Unknown",
    icon: UnknownIcon,
  },
  {
    id: "assault-bike",
    label: "Assault bike",
    icon: AssaultBikeIcon,
  },
  {
    id: "barbell",
    label: "Barbell",
    icon: BarbellIcon,
  },
  {
    id: "box",
    label: "Box",
    icon: BoxIcon,
  },
  {
    id: "jump-rope",
    label: "Jumping rope",
    icon: JumpRopeIcon,
  },
  {
    id: "dips-bar",
    label: "Dips bar",
    icon: DipsBarIcon,
  },
  {
    id: "rower",
    label: "Rower",
    icon: RowerIcon,
  },
  {
    id: "dumbell",
    label: "Dumbell",
    icon: DumbellIcon,
  },
  {
    id: "kettlebell",
    label: "Kettlebell",
    icon: KettlebellIcon,
  },
  {
    id: "medecine-ball",
    label: "Medecine ball",
    icon: MedecineBallIcon,
  },
  {
    id: "no-equipment",
    label: "No equipment",
    icon: NoEquipmentIcon,
  },
  {
    id: "pullup-bar",
    label: "Pullup bar",
    icon: PullupBarIcon,
  },
  {
    id: "wall",
    label: "Wall",
    icon: WallIcon,
  },
];
