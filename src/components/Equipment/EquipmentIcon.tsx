import { Equipments } from "./Equipment.constants";

interface EquipmentIconProps {
  equipmentId: Exercise["equipmentId"];
  modalityId: Exercise["modalityId"];
  width?: number;
  height?: number;
}

export default function EquipmentIcon(props: EquipmentIconProps) {
  const { equipmentId, modalityId, width = 34, height = 34 } = props;
  const equipment =
    Equipments.find((eq) => eq.id === equipmentId) ||
    Equipments.find((eq) => eq.id === "unknown");

  const EquipmentIcon = equipment?.icon;
  return (
    <div
      data-tip={equipment?.label}
      className={`tooltip tooltip-left tooltip-${modalityId?.toLowerCase()}`}
    >
      <div>
        <EquipmentIcon width={width} height={height} />
      </div>
    </div>
  );
}
