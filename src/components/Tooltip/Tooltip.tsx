type TooltipProps = {
  text: string;
  position?: "bottom" | "left" | "right" | "top";
  color?: string;
  children: JSX.Element;
};

export default function Tooltip(props: TooltipProps) {
  const { text, position = "top", color = "accent", children } = props;
  return (
    <div
      data-tip={text}
      className={`relative tooltip tooltip-${
        position ? position : "top"
      } tooltip-${color}`}
    >
      {children}
    </div>
  );
}
