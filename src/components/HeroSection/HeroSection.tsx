type HeroSection = {
  blob: JSX.Element;
  illustration: JSX.Element;
  headline: string;
  h1: String;
  description: String;
  actions: JSX.Element;
};

export default function HeroSection({
  blob,
  illustration,
  actions,
  h1,
  headline,
  description,
}: HeroSection) {
  return (
    <div className="headline relative flex flex-col gap-y-4 sm:flex-row-reverse">
      {blob}
      <div className="illustration flex justify-center lg:justify-between">
        {illustration}
      </div>
      <div className="relative">
        <div className="flex flex-col gap-y-5 md:w-5/6 lg:mt-28 xl:mt-44">
          <div className="uppercase text-gray-400 font-light md:text-lg tracking-wider">
            {headline}
          </div>
          <h1 className="text-white text-3xl max-w-sm font-extrabold leading-8 md:text-4xl lg:text-5xl lg:max-w-none">
            {h1}
          </h1>
          <p className="text-gray-300 font-extralight md:text-lg">
            {description}
          </p>
        </div>
        <div className="actions flex gap-x-3 mt-6 sm:mt-8 md:gap-x-6">
          {actions}
        </div>
      </div>
    </div>
  );
}
