import styles from "./ExerciseCard.module.scss";
import {
  LockClosedIcon,
  LightningBoltIcon,
  MenuAlt2Icon,
  TrashIcon,
  PencilIcon,
} from "@heroicons/react/solid";
import Tooltip from "components/Tooltip/Tooltip";
import EquipmentIcon from "components/Equipment/EquipmentIcon";
import ModalityIcon from "components/Modality/ModalityIcon";
import { useState } from "react";
import Modal from "components/Modal/Modal";
import Menu from "components/Menu/menu";

type ExerciseCardProps = {
  exercise?: Exercise;
  isSubmitting: boolean;
  onEditExercise?: (exercise: Exercise) => void;
  onDeleteExercise?: (exercise: Exercise) => void;
};

export default function ExerciseCard(props: ExerciseCardProps) {
  const { exercise, isSubmitting, onEditExercise, onDeleteExercise } = props;
  const isSkeleton = !exercise;
  const [confirmDeleteExerciseModal, set_confirmDeleteExerciseModal] =
    useState<boolean>(false);

  return (
    <>
      <div className={`${styles.card} ${isSkeleton && "skeleton"}`}>
        <div
          className={`${styles["used-in-workout"]} badge badge-${
            exercise && exercise.modalityId?.toLowerCase()
          } border-none`}
        >
          {exercise ? "0" : ""}
        </div>

        <div className={styles["status-icons"]}>
          {exercise && exercise.hasRepMax && (
            <Tooltip text="Has one rep max" position="left">
              <LightningBoltIcon className="text-base-content" width="22" />
            </Tooltip>
          )}
          {!exercise ? (
            <div className="skeleton icon" />
          ) : exercise.protected ? (
            <Tooltip text="Protected" position="left">
              <LockClosedIcon className="text-base-content" width="22" />
            </Tooltip>
          ) : (
            <Menu
              MenuIcon={
                <MenuAlt2Icon
                  className="cursor-pointer text-base-content"
                  width="24"
                />
              }
              options={[
                {
                  onClick: () =>
                    exercise && onEditExercise && onEditExercise(exercise),
                  label: "Edit",
                  Icon: <PencilIcon width="20" className="mr-2" />,
                },
                {
                  className: "bg-error",
                  onClick: () => set_confirmDeleteExerciseModal(true),
                  label: "Delete",
                  Icon: <TrashIcon width="20" className="mr-2" />,
                },
              ]}
            />
          )}
        </div>
        <div className={styles.information}>
          <div className={styles.name}>
            <p> The {exercise && exercise.name} </p>
          </div>
          <div className={styles.modality}>
            <div className="icon">
              {exercise && <ModalityIcon modalityId={exercise.modalityId} />}
            </div>
            <div className={styles.text}>
              <p>
                {!exercise
                  ? "Skeleton text"
                  : !exercise.modalityId
                  ? "Unknown"
                  : exercise.modalityId}
              </p>
            </div>
          </div>
          <div className={`${styles["equipment-icon"]} icon`}>
            {exercise && (
              <EquipmentIcon
                modalityId={exercise.modalityId}
                equipmentId={exercise.equipmentId}
              />
            )}
          </div>
        </div>
      </div>
      {exercise && confirmDeleteExerciseModal && (
        <Modal>
          <>
            <p>Are you sure you want to delete the exercise {exercise.name}?</p>
            <div className="modal-action">
              <button
                type="submit"
                className={`btn btn-error text-base-content ${
                  isSubmitting ? "loading" : ""
                }`}
                onClick={() =>
                  exercise && onDeleteExercise && onDeleteExercise(exercise)
                }
              >
                Yes, delete
              </button>
              <button
                type="button"
                className="btn btn-ghost"
                onClick={() => set_confirmDeleteExerciseModal(false)}
              >
                Close
              </button>
            </div>
          </>
        </Modal>
      )}
    </>
  );
}
