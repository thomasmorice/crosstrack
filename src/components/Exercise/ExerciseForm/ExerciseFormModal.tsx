import Label from "components/Form/Label";
import TextInput from "components/Form/TextInput";
import SelectInput from "components/Form/SelectInput";
import Toggle from "components/Form/Toggle";
import Modal from "components/Modal/Modal";
import { Modality } from "@prisma/client";
import {
  EquipmentType,
  Equipments,
} from "components/Equipment/Equipment.constants";
import * as Yup from "yup";
import { Formik } from "formik";
import { exerciseService } from "services/exercise.service";
import { useContext } from "react";
import { ToastContext } from "components/Toast/ToastContext";

type ModalityOptionType = {
  id: Modality;
  modality: string;
};

const Modalities: ModalityOptionType[] = Object.values(Modality).map(
  (modality) => {
    return {
      id: modality,
      modality:
        modality.charAt(0).toUpperCase() + modality.slice(1).toLowerCase(),
    };
  }
);

const ExerciseValidationSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "Too Short!")
    .max(48, "Too Long!")
    .required("Required"),
  equipmentId: Yup.mixed()
    .oneOf(
      Equipments.map((e) => e.id),
      // ["unknown"],
      "One equipment must be selected"
    )
    .required("Select an equipment"),
});

type ExerciseFormModalProps = {
  exerciseToEdit?: Exercise;
  onClose: () => void;
  onSaved?: (exercise: Exercise) => void;
};

export default function ExerciseFormModal(props: ExerciseFormModalProps) {
  const { exerciseToEdit, onClose, onSaved } = props;
  const { addToast } = useContext(ToastContext);
  return (
    <div className="relative">
      <Modal>
        <>
          <h2 className="text-2xl mb-6"> Add an exercise </h2>
          <Formik<Exercise>
            enableReinitialize
            initialValues={
              !exerciseToEdit
                ? exerciseService.getEmptyExercise()
                : exerciseToEdit
            }
            validationSchema={ExerciseValidationSchema}
            onSubmit={async (exercise, actions) => {
              actions.setSubmitting(true);
              const response = (await !exercise.id)
                ? exerciseService.addExercise(exercise)
                : exerciseService.editExercise(exercise);

              response
                .then((json) => {
                  if (!json.success) {
                    actions.setErrors({
                      name: json.message,
                    });
                  } else {
                    actions.resetForm();
                    addToast({
                      message: `${exercise.name} has been ${
                        !exercise.id ? "added" : "edited"
                      } successfully`,
                    });
                    let newExercise: Exercise = json.response;
                    exerciseService.refetchUserExercise();
                    onSaved && onSaved(newExercise);
                    onClose();
                  }
                  actions.setSubmitting(false);
                })
                .catch((e: Error) => {
                  console.log("error ", e);
                });
            }}
          >
            {({
              handleChange,
              handleSubmit,
              values,
              touched,
              errors,
              isSubmitting,
            }) => {
              return (
                <form className="form-control gap-y-3" onSubmit={handleSubmit}>
                  <div>
                    <Label isRequired={true}> Name </Label>
                    <TextInput
                      inputProps={{
                        id: "name",
                        placeholder: "Deadlift",
                        onChange: handleChange,
                        type: "primary",
                        value: values.name,
                        error: errors.name,
                      }}
                      prepend="The"
                    />
                  </div>
                  <div>
                    <Label> Modality </Label>
                    <SelectInput<ModalityOptionType>
                      id="modalityId"
                      placeholder="Select a modality"
                      options={{
                        array: Modalities,
                        id: "id",
                        label: "modality",
                      }}
                      type="bordered"
                      onChange={handleChange}
                      value={values.modalityId || ""}
                    />
                  </div>
                  <div>
                    <Label> Equipment </Label>
                    <SelectInput<EquipmentType>
                      id="equipmentId"
                      defaultValue="unknown"
                      placeholder="Select an equipment"
                      options={{
                        array: Equipments.filter((e) => e.id !== "unknown"),
                        id: "id",
                        label: "label",
                      }}
                      onChange={handleChange}
                      value={values.equipmentId}
                      error={errors.equipmentId}
                    />
                  </div>
                  <div>
                    <Toggle
                      id="hasRepMax"
                      label="Has one rep max"
                      type="accent"
                      checked={values.hasRepMax ? values.hasRepMax : false}
                      onChange={handleChange}
                    />
                  </div>

                  <div className="modal-action">
                    <button
                      type="submit"
                      className={`btn btn-primary transition-all ${
                        isSubmitting ? "btn-circle loading" : ""
                      }`}
                    >
                      {!isSubmitting ? "Save" : ""}
                    </button>
                    <button
                      type="button"
                      className="btn btn-ghost"
                      onClick={onClose}
                    >
                      Close
                    </button>
                  </div>
                </form>
              );
            }}
          </Formik>
        </>
      </Modal>
    </div>
  );
}
