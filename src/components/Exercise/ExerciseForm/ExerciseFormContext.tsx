import { createContext, useCallback, useState } from "react";
import ExerciseFormModal from "components/Exercise/ExerciseForm/ExerciseFormModal";

const defaultApi = {
  isVisible: false,
  savedExercise: {} as Exercise,
  createExercise: () => null,
  editExercise: (exercise: Exercise) => null,
  resetSavedExercise: () => null,
  closeWorkoutEdition: () => null,
};

export type ExerciseFormContextApi = typeof defaultApi;

/**
 * Create Context
 */
export const ExerciseFormContext =
  createContext<ExerciseFormContextApi>(defaultApi);

export function ExerciseFormProvider({ children }: any) {
  const [isVisible, set_isVisible] = useState<boolean>(defaultApi.isVisible);
  const [exerciseToEdit, set_exercisToEdit] = useState<Exercise>();
  const [savedExercise, set_savedExercise] = useState<Exercise>();

  const createExercise = useCallback(() => {
    set_isVisible(true);
  }, [isVisible, set_isVisible]);

  const editExercise = useCallback(
    (exercise: Exercise) => {
      set_exercisToEdit(exercise);
      set_isVisible(true);
    },
    [isVisible, set_isVisible]
  );

  const resetSavedExercise = useCallback(() => {
    set_savedExercise(undefined);
  }, []);

  const closeWorkoutEdition = useCallback(() => {
    set_isVisible(false);
  }, [isVisible, set_isVisible]);

  // Return Provider with full API
  const api = {
    createExercise,
    editExercise,
    closeWorkoutEdition,
    savedExercise,
    resetSavedExercise,
  };
  return (
    <ExerciseFormContext.Provider value={api as ExerciseFormContextApi}>
      <>
        {isVisible && (
          <ExerciseFormModal
            onClose={() => set_isVisible(false)}
            exerciseToEdit={exerciseToEdit}
            onSaved={(exercise) => set_savedExercise(exercise)}
          />
        )}
        {children}
      </>
    </ExerciseFormContext.Provider>
  );
}
