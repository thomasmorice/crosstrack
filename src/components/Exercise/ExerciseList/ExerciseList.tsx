import { motion, AnimatePresence } from "framer-motion";
import ExerciseCard from "../ExerciseCard/ExerciseCard";

type ExerciseListProps = {
  onEditExercise: (exercise: Exercise) => void;
  onDeleteExercise: (exercise: Exercise) => void;
  isSubmitting: boolean;
  exercises?: Exercise[];
};

export default function ExerciseList({
  exercises,
  isSubmitting,
  onEditExercise,
  onDeleteExercise,
}: ExerciseListProps) {
  return (
    <motion.div
      className="flex flex-wrap gap-10"
      initial="hidden"
      animate="visible"
      variants={{
        visible: {
          opacity: 1,
          transition: {
            when: "beforeChildren",
            staggerChildren: 0.05,
          },
        },
        hidden: {
          opacity: 0,
          transition: {
            when: "afterChildren",
          },
        },
      }}
    >
      <AnimatePresence>
        {exercises &&
          exercises.map((exercise) => (
            <motion.div
              key={exercise.id}
              variants={{
                visible: { opacity: 1, y: 0 },
                hidden: { opacity: 0, y: 100 },
              }}
            >
              <ExerciseCard
                onDeleteExercise={(exercise) => onDeleteExercise(exercise)}
                isSubmitting={isSubmitting}
                onEditExercise={(exercise) => onEditExercise(exercise)}
                exercise={exercise}
              />
            </motion.div>
          ))}
      </AnimatePresence>
    </motion.div>
  );
}
