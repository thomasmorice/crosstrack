import WeightliftingIcon from "./images/weightlifting.svg";
import CardioIcon from "./images/cardio.svg";
import GymnasticIcon from "./images/gymnastic.svg";

interface ModalityIconProps {
  modalityId: Exercise["modalityId"];
  className?: string;
}

export default function EquipmentIcon(props: ModalityIconProps) {
  const { modalityId, className } = props;

  return (
    <div
      className={`${className}`}
      style={{
        width: "34px",
        maxHeight: "34px",
      }}
    >
      {modalityId === "CARDIO" && (
        <CardioIcon className={`${className} icon icon-cardio h-7`} />
      )}
      {modalityId === "GYMNASTIC" && (
        <GymnasticIcon className={`${className} icon icon-gymnastic h-9`} />
      )}
      {modalityId === "WEIGHTLIFTING" && (
        <WeightliftingIcon
          className={`${className} icon icon-weightlifting h-5`}
        />
      )}
      {!modalityId && (
        <div
          className={`flex justify-center leading-7 h-7 w-7 rounded-full bg-base-100 text-gray-400`}
        >
          {" "}
          ?{" "}
        </div>
      )}
    </div>
  );
}
