import { useEffect, useState } from "react";
import { PieChart } from "react-minimal-pie-chart";
import { getCssVariableInJs } from "utils/utils";
import Tooltip from "components/Tooltip/Tooltip";
import { Modality } from "@prisma/client";

type WorkoutModalityShare = {
  workout: Workout;
};

export default function WorkoutModalityShare({
  workout,
}: WorkoutModalityShare) {
  const [modalityDataShare, set_modalityDataShare] = useState<
    {
      key: string;
      title: string;
      value: number;
      color: string;
    }[]
  >([]);

  const [hasNoModalities, set_hasNoModalities] = useState<boolean>(false);
  const [totalValues, set_totalValues] = useState<number>(0);
  const [tooltipData, set_tooltipData] = useState<{
    color?: string;
    text: string;
  }>({
    text: "",
  });

  const updateShares = () => {
    let tempTotalValues = 0;
    let tempModalityDataShare = Object.keys(Modality).map((modality) => ({
      key: modality,
      title: modality.toLowerCase(),
      value: 0,
      color: "",
    }));
    set_totalValues(0);
    set_hasNoModalities(true);
    workout.workoutElements.map((element) => {
      element.workoutExercises.map((workoutExercise) => {
        set_hasNoModalities(false);
        let share = tempModalityDataShare.find(
          (share) => share.key === workoutExercise.exercise?.modalityId
        );
        if (share) {
          share.value++;
          tempTotalValues++;
          if (!share.color) {
            share.color = getCssVariableInJs(
              `--${share.key.toLowerCase()}-color`
            );
          }
        }
      });
    });

    set_totalValues(tempTotalValues);
    set_modalityDataShare(tempModalityDataShare);
  };

  useEffect(() => {
    updateShares();
  }, []);

  useEffect(() => {
    updateShares();
  }, [workout]);

  return (
    <>
      {process.browser && (
        <Tooltip {...tooltipData}>
          <PieChart
            onMouseOver={(e, index) => {
              if (totalValues > 0) {
                set_tooltipData({
                  text: `${modalityDataShare[index].title}: ${Math.round(
                    (modalityDataShare[index].value * 100) / totalValues
                  )}%`,
                  color: modalityDataShare[index].title,
                });
              } else {
                set_tooltipData({
                  text: "No exercises",
                });
              }
            }}
            radius={44}
            lineWidth={37}
            data={
              hasNoModalities
                ? [
                    {
                      color: "#606060",
                      value: 1,
                      key: "empty",
                    },
                  ]
                : modalityDataShare
            }
          />
        </Tooltip>
      )}
    </>
  );
}
