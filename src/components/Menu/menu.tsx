type MenuProps = {
  position?: "left" | "top" | "right" | "end";
  MenuIcon: JSX.Element;
  options?: {
    Icon?: JSX.Element;
    className?: string;
    label: string;
    onClick: () => void;
  }[];
};

export default function Menu({
  position = "left",
  MenuIcon,
  options,
}: MenuProps) {
  return (
    <div className={`dropdown dropdown-${position} `}>
      <div tabIndex={0} className="">
        {MenuIcon}
      </div>
      <ul
        tabIndex={0}
        className="p-2 shadow menu dropdown-content bg-base-100 rounded-box w-52 text-base-content"
      >
        {options?.map((option, index) => (
          <li key={index}>
            <a onClick={option.onClick} className={option.className}>
              {option.Icon} {option.label}
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
}
