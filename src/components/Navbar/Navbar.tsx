import { ReactElement, useState } from "react";
import Link from "next/link";
import { MenuIcon } from "@heroicons/react/outline";
import { motion, AnimatePresence } from "framer-motion";
import Avatar from "components/User/Avatar";
import { useRouter } from "next/router";

export type NavbarLinkType = {
  title: string;
  url: string;
};

type NavbarProps = {
  application: {
    logo: ReactElement;
    name: string;
  };
  user: {
    signIn: () => void;
    signOut: () => void;
    loading: boolean;
    avatar?: string;
  };
  links: NavbarLinkType[];
};

export default function Navbar(props: NavbarProps) {
  const { application, user, links } = props;
  const [isMenuOpen, set_isMenuOpen] = useState(false);
  const router = useRouter();
  return (
    <div className="relative navbar-and-mobile-menu flex flex-col m-2">
      <div className="navbar z-10 shadow-lg bg-neutral text-neutral-content justify-around rounded-box py-0 px-4">
        <div className="flex w-full justify-between">
          <div className="flex items-center gap-x-8">
            <div className="flex text-lg font-bold gap-x-3">
              <Link href="/">
                <a className="flex gap-x-2">
                  {application.logo}
                  {application.name}
                </a>
              </Link>
            </div>
            <div className="hidden gap-x-3 md:flex">
              {links.map((link) => {
                const isCurrentPage = router.asPath.includes(link.url);
                return (
                  <Link key={link.url} href={link.url}>
                    <a
                      suppressHydrationWarning={true}
                      className={`btn btn-sm rounded-btn ${
                        isCurrentPage ? "btn-primary" : "btn-ghost"
                      }`}
                    >
                      {link.title}
                    </a>
                  </Link>
                );
              })}
            </div>
          </div>

          <div className="flex items-center gap-x-2">
            {user.loading ? (
              <button className="btn loading btn-circle btn-lg bg-base-200 btn-ghost text-primary"></button>
            ) : user.avatar ? (
              <Avatar
                img={user.avatar}
                menu={[
                  {
                    label: "Sign out",
                    onClick: user.signOut,
                  },
                ]}
              />
            ) : (
              <button onClick={user.signIn} className="btn btn-primary">
                Log in
              </button>
            )}
            <button
              onClick={() => set_isMenuOpen(!isMenuOpen)}
              className="flex btn btn-square btn-ghost md:hidden"
            >
              <MenuIcon className="h-8" />
            </button>
          </div>
        </div>
      </div>

      {/* Mobile menu opened */}
      <AnimatePresence>
        {isMenuOpen && (
          <motion.ul
            initial={{ y: -5, opacity: 0 }}
            animate={{ y: 5, opacity: 1 }}
            exit={{
              y: -5,
              opacity: 0,
              transition: {
                duration: 0.1,
              },
            }}
            onClick={() => {
              set_isMenuOpen(false);
            }}
            className="menu z-0 py-3 shadow-lg bg-base-200 rounded-box"
          >
            <li className="menu-title">
              <span>Links</span>
            </li>
            {links.map((link) => (
              <li key={link.url}>
                <Link key={link.url} href={link.url}>
                  <a>{link.title}</a>
                </Link>
              </li>
            ))}
          </motion.ul>
        )}
      </AnimatePresence>
    </div>
  );
}
