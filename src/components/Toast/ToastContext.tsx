import { v4 as uuid } from "uuid";
import useInterval from "hooks/useInterval";
import { createContext, useCallback, useState } from "react";
import { AnimatePresence } from "framer-motion";
import { Toast } from "./Toast";

const INTERVAL = 800;
const DEFAULT_TIMEOUT = 3500;

export type TOAST_VARIANTS = "success" | "error" | "warning" | "info";

export interface Toast {
  id: string;
  message: string | React.ReactNode;
  variant: TOAST_VARIANTS;
  timestamp: number;
  timeout?: number | null; // pass in null for no timeout
}

interface AddToast {
  id?: string;
  message: string;
  variant?: TOAST_VARIANTS;
  timeout?: Toast["timeout"];
}

const defaultApi = {
  toasts: [] as Toast[],
  clearToast: (id?: string | string[]) => null,
  addToast: (toast: Partial<AddToast>) => null,
};

export type ToastsContextApi = typeof defaultApi;

/**
 * Create Context
 */
export const ToastContext = createContext<ToastsContextApi>(defaultApi);

/**
 * Custom Notifications Provider
 */
export function ToastsProvider({ children }: any) {
  // Notifications queue is managed in local useState
  const [toasts, setToasts] = useState<Toast[]>(defaultApi.toasts);

  // Method to push a new notification
  const addToast = useCallback(
    (toast: AddToast) => {
      const existing = toasts.find((t) => t.id === toast.id);
      const nextToast = existing
        ? toasts.map((t) => (t.id === toast.id ? { ...existing, ...toast } : t))
        : toasts.concat({
            id: uuid(),
            timestamp: new Date().getTime(),
            variant: "success",
            ...toast,
          });
      setToasts(nextToast);
    },
    [toasts, setToasts]
  );

  // Method to clear a notification
  const clearToast = useCallback(
    (id?: string | string[]) => {
      if (!id) {
        setToasts([]);
      } else {
        const ids = Array.isArray(id) ? id : [id];
        const nextToasts = toasts.filter(({ id }) => !ids.includes(id));
        setToasts(nextToasts);
      }
    },
    [toasts, setToasts]
  );

  // Set up interval to auto-expire notifications
  const handleExpireToast = useCallback(
    (currentTime) => {
      if (toasts.length) {
        const expiredIds = toasts.reduce((acc: string[], toast) => {
          const isExpired =
            toast.timestamp <= currentTime - (toast.timeout || DEFAULT_TIMEOUT);
          return isExpired && toast.timeout !== null
            ? acc.concat(toast.id)
            : acc;
        }, []);
        if (expiredIds.length) {
          clearToast(expiredIds);
        }
      }
    },
    [toasts, clearToast]
  );

  useInterval(handleExpireToast, INTERVAL);

  // Return Provider with full API
  const api = { toasts, addToast, clearToast };
  return (
    <ToastContext.Provider value={api as ToastsContextApi}>
      <>
        <div className="fixed z-50 bottom-5 right-10">
          <AnimatePresence>
            {toasts &&
              toasts.map((toast) => {
                return <Toast key={`toast-${toast.id}`} toast={toast} />;
              })}
          </AnimatePresence>
        </div>
        {children}
      </>
    </ToastContext.Provider>
  );
}
