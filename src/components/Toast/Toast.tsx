import { ReactNode, useContext } from "react";
import { XCircleIcon } from "@heroicons/react/solid";
import { ToastContext, Toast as ToastType } from "./ToastContext";
import { motion } from "framer-motion";

type ToastProps = {
  toast: ToastType;
};

export function Toast({ toast }: ToastProps) {
  const { clearToast } = useContext(ToastContext);
  return (
    <motion.div
      initial={{ y: 10, opacity: 0 }}
      animate={{ y: 0, opacity: 1 }}
      exit={{
        y: 10,
        opacity: 0,
        transition: {
          duration: 0.1,
        },
      }}
      className={`alert alert-${toast.variant} mt-2`}
    >
      <div className="flex max-w-sm justify-between">
        <label>{toast.message}</label>
        <XCircleIcon
          onClick={() => {
            clearToast(toast.id);
          }}
          className="h-6 cursor-pointer ml-4"
        />
      </div>
    </motion.div>
  );
}
