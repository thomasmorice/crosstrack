type AvatarProps = {
  img?: string;
  menu?: {
    label: string;
    onClick: () => void;
  }[];
};

export default function Avatar(props: AvatarProps) {
  const { img, menu } = props;

  return (
    <div className="dropdown dropdown-end">
      <div tabIndex={0} className="avatar align-middle">
        <div className="btn btn-circle btn-ghost h-[3.25rem] w-[3.25rem]">
          <img
            referrerPolicy="no-referrer"
            className="rounded-full p-1"
            src={img}
          />
        </div>
      </div>
      {menu && (
        <ul
          tabIndex={0}
          className="p-2 shadow menu dropdown-content bg-base-300 rounded-box w-52"
        >
          {menu.map((item) => (
            <li key={item.label}>
              <a onClick={item.onClick}>{item.label}</a>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}
