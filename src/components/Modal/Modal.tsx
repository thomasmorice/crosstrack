type ModalProps = {
  isVisible?: boolean;
  actions?: {
    close: {
      label: string;
      onClick: () => void;
    };
    accept: {
      label: string;
      onClick: () => void;
    };
  };
  children: JSX.Element;
};

export default function Modal(props: ModalProps) {
  const { isVisible = true, actions, children } = props;
  return (
    <div className={`modal ${isVisible ? "modal-open" : ""}`}>
      <div className="modal-box bg-base-200">
        <div className="form-control">{children}</div>
        {actions && (
          <div className="modal-action">
            <a className="btn btn-primary" onClick={actions.accept.onClick}>
              {actions.accept.label}
            </a>
            <a
              className="btn btn-ghost hover:text-base-content"
              onClick={actions.close.onClick}
            >
              {actions.close.label}
            </a>
          </div>
        )}
      </div>
    </div>
  );
}
