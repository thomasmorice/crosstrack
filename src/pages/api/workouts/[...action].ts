import type { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/client";
import prisma from "lib/prisma";
import {
  Prisma,
  // Workout,
  // WorkoutElement,
  // WorkoutExercise,
} from "@prisma/client";

// interface WorkoutFullObj extends Workout {
//   workoutElements: WorkoutElementWithExercises[];
// }

// interface WorkoutElementWithExercises extends WorkoutElement {
//   workoutExercises: WorkoutExercise[];
// }

// Exercise /api/workout/ POST
export default async function handle(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const session = await getSession({ req });
  if (!session) {
    res.status(401).end();
  } else {
    if (req.method === "POST") {
      const workout: Workout = { ...req.body };

      if (workout.id && workout.creatorId !== session.user.id) {
        res.statusMessage = "Unauthorized - cannot edit this workout";
        res.status(401).end();
      } else {
        handleAddOrEdit(workout);
      }
    } else if (req.method === "DELETE") {
      const workoutId = req.query.action[1];
      workoutId && handleDELETE(workoutId, res);
    }
  }

  // DELETE /api/workout/delete/:id
  async function handleDELETE(workoutId: string, res: NextApiResponse<any>) {
    const workout = await prisma.workout.delete({
      where: { id: Number(workoutId) },
    });
    res.json(workout);
    res.status(200).end();
  }

  async function handleAddOrEdit(workout: Workout) {
    try {
      if (workout.id) {
        // delete workout elements and exercises to recreate them after
        await prisma.workoutElement.deleteMany({
          where: {
            workoutId: workout.id,
          },
        });
      }

      const workoutCreateInput: Prisma.WorkoutCreateInput = {
        ...workout,
        name: workout.name || "",
        type: workout.type || "",
        description: workout.description || "",
        creatorId: session?.user.id || null,
        workoutElements: {
          create: workout.workoutElements.map((workoutElement, index) => {
            return {
              annotation: workoutElement.annotation || "",
              order: index,
              transitionAnnotation: workoutElement.transitionAnnotation,
              workoutExercises: {
                create: workoutElement.workoutExercises.map(
                  (workoutExercise, index) => ({
                    exerciseId: workoutExercise.exercise?.id || 1,
                    annotation: workoutExercise.annotation,

                    order: index,
                    reps: workoutExercise.reps || "",
                  })
                ),
              },
            };
          }),
        },
      };

      // update workout
      const newOrUpdatedWorkout = await prisma.workout.upsert({
        create: workoutCreateInput,
        update: workoutCreateInput,
        where: {
          id: workout.id ? workout.id : 0,
        },
        include: {
          workoutElements: {
            include: {
              workoutExercises: {
                include: {
                  exercise: true,
                },
              },
            },
          },
        },
      });

      res.status(200).json({
        success: true,
        response: newOrUpdatedWorkout,
      });
    } catch (e) {
      console.log("error", e);
      // if (e instanceof Prisma.PrismaClientKnownRequestError) {
      //   if (e.code === "P2002") {
      //     res.status(409).json({
      //       success: false,
      //       message: `This exercise already exists in the database`,
      //     });
      //   }
      // }
    }
  }
}
