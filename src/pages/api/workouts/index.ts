import type { NextApiRequest, NextApiResponse } from "next";
import prisma from "lib/prisma";
import { getSession } from "next-auth/client";

export default async function handle(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const session = await getSession({ req });
  if (!session) {
    res.status(401).end();
  } else {
    if (req.method === "GET") {
      handleGetAllUserWorkouts();
    } else {
      throw new Error(
        `The HTTP ${req.method} method is not supported at this route.`
      );
    }
  }

  async function handleGetAllUserWorkouts() {
    const workouts = await prisma.workout.findMany({
      where: {
        creatorId: session?.user.id,
      },
      orderBy: [
        {
          createdAt: "desc",
        },
        {
          id: "asc",
        },
      ],
      include: {
        workoutElements: {
          orderBy: {
            order: "asc",
          },
          include: {
            workoutExercises: {
              orderBy: {
                order: "asc",
              },
              include: {
                exercise: true,
              },
            },
          },
        },
      },
    });
    res.status(200).json(workouts);
  }
}
