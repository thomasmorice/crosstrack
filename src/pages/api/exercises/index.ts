import type { NextApiRequest, NextApiResponse } from "next";
import prisma from "lib/prisma";
import { getSession } from "next-auth/client";

export default async function handle(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const session = await getSession({ req });
  if (!session) {
    res.status(401).end();
  } else {
    if (req.method === "GET") {
      handleGetAllUserExercises();
    } else {
      throw new Error(
        `The HTTP ${req.method} method is not supported at this route.`
      );
    }
  }

  async function handleGetAllUserExercises() {
    const exercises = await prisma.exercise.findMany({
      where: {
        creator: {
          id: session?.user.id,
        },
      },
    });
    res.status(200).json(exercises);
  }
}
