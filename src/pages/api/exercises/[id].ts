import type { NextApiRequest, NextApiResponse } from "next";
import prisma from "lib/prisma";
import { getSession } from "next-auth/client";

export default async function handle(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const session = await getSession({ req });
  const exerciseId = req.query.id as string;
  if (!session) {
    res.status(401).end();
  } else {
    const exercise = await prisma.exercise.findFirst({
      where: {
        id: parseInt(exerciseId),
      },
    });

    if (req.method === "DELETE") {
      if (!exercise) {
        throw new Error(`Exercise cannot be found`);
      } else if (exercise.creatorId !== session.user.id) {
        res.statusMessage = "Unauthorized to delete this exercise";
        res.status(401).end();
      } else {
        handleDELETE(exerciseId, res);
      }
    } else if (req.method === "GET") {
      handleGET(exerciseId, res);
    } else {
      throw new Error(
        `The HTTP ${req.method} method is not supported at this route.`
      );
    }
  }
}

//GET /api/exercises/:id
async function handleGET(exerciseId: string, res: NextApiResponse<any>) {
  const exercise = await prisma.exercise.findUnique({
    where: { id: Number(exerciseId) },
    // include: { creator: true },
  });
  res.json(exercise);
  res.end();
}

// DELETE /api/exercises/:id
async function handleDELETE(exerciseId: string, res: NextApiResponse<any>) {
  const exercise = await prisma.exercise.delete({
    where: { id: Number(exerciseId) },
  });
  res.json(exercise);
  res.status(200).end();
}
