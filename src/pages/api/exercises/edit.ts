import type { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/client";
import prisma from "lib/prisma";
import { Exercise, Prisma } from "@prisma/client";

// Exercise /api/post
export default async function handle(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const session = await getSession({ req });
  if (!session) {
    res.status(401).end();
  } else {
    if (req.method === "POST") {
      const exercise: Exercise = { ...req.body };
      if (exercise.creatorId !== session.user.id) {
        res.statusMessage = "Unauthorized - cannot edit this exercise";
        res.status(401).end();
      } else {
        handleEdit(exercise);
      }
    }
  }

  async function handleEdit(exercise: Exercise) {
    try {
      const result = await prisma.exercise.update({
        where: {
          id: exercise.id,
        },
        data: {
          ...exercise,
        },
      });
      res.status(200).json({
        success: true,
        response: result,
      });
    } catch (e) {
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        if (e.code === "P2002") {
          res.status(409).json({
            success: false,
            message: `This exercise already exists in the database`,
          });
        }
      }
    }
  }
}
