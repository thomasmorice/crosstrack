import type { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/client";
import prisma from "lib/prisma";

export default async function handle(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const term = req.query.term as string;
  const session = await getSession({ req });

  if (!session) {
    throw new Error(`Unauthenticate users aren't allowed on this route`);
  } else {
    if (req.method === "GET") {
      handleSearch(term);
    } else {
      throw new Error(
        `The HTTP ${req.method} method is not supported at this route.`
      );
    }
  }

  // SEARCH /api/exercises/:term
  async function handleSearch(term: string) {
    const exercises = await prisma.exercise.findMany({
      where: {
        AND: [
          {
            name: {
              contains: term,
              mode: "insensitive",
            },
          },
          {
            OR: [{ protected: true }, { creatorId: session?.user.id }],
          },
        ],
      },
    });
    res.json(exercises);
    res.end();
  }
}
