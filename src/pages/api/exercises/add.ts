import type { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/client";
import prisma from "lib/prisma";
import { Exercise } from "@prisma/client";
import { Prisma } from "@prisma/client";

// Exercise /api/post
export default async function handle(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const session = await getSession({ req });
  if (!session) {
    res.status(401).end();
  } else {
    if (req.method === "POST") {
      handleAdd(req);
    }
  }

  async function handleAdd(req: NextApiRequest) {
    const exercise: Exercise = { ...req.body };
    exercise.creatorId = session && session.user.id;
    console.log("exercise", exercise);
    try {
      const result = await prisma.exercise.create({
        data: {
          ...exercise,
        },
      });
      res.status(200).json({
        success: true,
        response: result,
      });
    } catch (e) {
      console.log("e", e);
      if (e instanceof Prisma.PrismaClientKnownRequestError) {
        if (e.code === "P2002") {
          res.status(409).json({
            success: false,
            message: `This exercise already exists in the database`,
          });
        }
      }
    }
  }
}
