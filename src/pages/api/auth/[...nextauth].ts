import { NextApiRequest, NextApiResponse } from "next";
import prisma from "lib/prisma";
import NextAuth, { Session } from "next-auth";
import Providers from "next-auth/providers";
import { PrismaAdapter } from "@next-auth/prisma-adapter";

const options = {
  providers: [
    Providers.Google({
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    }),
  ],
  callbacks: {
    session: async (session: Session, user: { id: string; name: string }) => {
      session.user.id = user.id;
      session.user.name = user.name;
      return Promise.resolve(session);
    },
    async jwt(token: { isAdmin: boolean }, user: { email: string }) {
      if (user) {
        // User object only passed on initial JWT creation
        const administrators = ["t.morice4@gmail.com"];
        token.isAdmin = administrators.includes(user?.email);
      }
      return token;
    },
  },
  adapter: PrismaAdapter(prisma),
};

export default NextAuth(options);
