import { useState, useEffect, useContext } from "react";
import type { NextPage, GetStaticProps } from "next";
import prisma from "lib/prisma";
import Head from "next/head";
import { signIn, useSession } from "next-auth/client";
import HeroSection from "components/HeroSection/HeroSection";
import Blob from "../../public/images/workouts/blob.svg";
import { workoutService } from "services/workout.service";
import WorkoutCard from "components/Workout/WorkoutCard/WorkoutCard";
import WorkoutForm from "components/Workout/WorkoutForm/WorkoutForm";
import { motion } from "framer-motion";

type WorkoutsPageProps = {
  // adminExercises: Exercise[];
};

// export async function getStaticProps(context: GetStaticProps) {
//   const adminExercises = await prisma.exercise.findMany({
//     where: {
//       protected: true,
//     },
//   });
//   return { props: { adminExercises } };
// }

const WorkoutsPage: NextPage<WorkoutsPageProps> = ({}) => {
  const [session, loading] = useSession();

  const [workouts, set_workouts] = useState<Workout[]>([]);
  const [newWorkout, set_newWorkout] = useState<Workout>(
    workoutService.getEmptyWorkout()
  );
  const [showNewWorkoutCard, set_showNewWorkoutCard] = useState<boolean>(false);

  const { data: userWorkouts, error: getUserWorkoutsError } =
    workoutService.getUserWorkouts(session?.user?.id);

  useEffect(() => {
    set_workouts(userWorkouts || []);
  }, [userWorkouts]);

  const createNewWorkout = () => {
    set_newWorkout(workoutService.getEmptyWorkout());
    set_showNewWorkoutCard(true);
  };

  useEffect(() => {}, [workouts]);

  return (
    <div>
      <Head>
        <title>Crosstrack - Workouts</title>
        <meta name="description" content="A workout tracker app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="p-4 sm:py-12 mx-auto">
        {!loading && !session && (
          <div className="mb-20">
            <HeroSection
              h1="Find out about our exercises and add your own"
              blob={<Blob className="absolute md:w-11/12 md:-top-16" />}
              illustration={
                <img
                  className="relative object-contain w-3/6 sm:w-full lg:w-full lg:-ml-24 lg:mt-10"
                  src="/images/workouts/workouts-illustration.png"
                />
              }
              headline="Don't just pray for it, work for it"
              description="This list is used to create workouts, most of the exercises are already available, but if you want to add your own you can do so by clicking on the button below"
              actions={
                <button
                  onClick={() => signIn()}
                  disabled={loading}
                  className={`btn btn-primary btn-${loading ? "disabled" : ""}`}
                >
                  Login to add your own exercises
                </button>
              }
            />
          </div>
        )}
        <button onClick={createNewWorkout} className="btn btn-primary mb-10">
          Create a workout
        </button>
        <motion.div className="flex gap-x-8 gap-y-10 flex-wrap">
          {workouts.map((workout, index) => (
            <WorkoutForm key={`workout-${index}`} workout={workout} />
          ))}
        </motion.div>
        {showNewWorkoutCard && (
          <WorkoutForm
            workout={newWorkout}
            handleCancelCreateNewWorkout={() => set_showNewWorkoutCard(false)}
          />
        )}
      </div>
    </div>
  );
};

export default WorkoutsPage;
