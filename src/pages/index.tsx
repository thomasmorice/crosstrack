import HeroSection from "components/HeroSection/HeroSection";
import type { NextPage } from "next";
import Head from "next/head";
import Blob from "../../public/images/home/blob.svg";
import { signIn } from "next-auth/client";

const Home: NextPage = () => {
  return (
    <div>
      <Head>
        <title>Crosstrack - Homepage</title>
        <meta name="description" content="A workout tracker app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="p-4 sm:py-12 mx-auto">
        <HeroSection
          headline="Train smarter, get stronger"
          h1="Follow your workout progress seamlessly."
          description="Whether you just wanna start working out, or you are trying to
          improve on your current program, Crosstrack will help you plan your
          workouts and follow your progress throughout your journey."
          illustration={
            <img
              className="relative object-contain w-4/6 sm:w-full lg:w-5/6"
              src="/images/home/homepage-crossfit-illustration.png"
            />
          }
          blob={
            <Blob className="absolute top-0 md:w-10/12 md:left-5 md:-mt-6" />
          }
          actions={
            <>
              <button onClick={() => signIn()} className="btn btn-primary">
                Sign up
              </button>
              <button className="btn btn-outline">Discover workouts</button>
            </>
          }
        />
      </div>
    </div>
  );
};

export default Home;
