import { useState, useEffect, useContext } from "react";
import type { NextPage, GetStaticProps } from "next";
import prisma from "lib/prisma";
import Head from "next/head";
import { signIn, useSession } from "next-auth/client";
import HeroSection from "components/HeroSection/HeroSection";
import Blob from "../../public/images/exercises/blob.svg";
import { exerciseService } from "services/exercise.service";
import { ToastContext } from "components/Toast/ToastContext";
import ExerciseList from "components/Exercise/ExerciseList/ExerciseList";
import { ExerciseFormContext } from "components/Exercise/ExerciseForm/ExerciseFormContext";

type ExercisesPageProps = {
  adminExercises: Exercise[];
};

export async function getStaticProps(context: GetStaticProps) {
  const adminExercises = await prisma.exercise.findMany({
    where: {
      protected: true,
    },
  });
  return { props: { adminExercises } };
}

const tabs: Tab[] = [
  {
    id: 1,
    label: "Default exercises",
  },
  {
    id: 2,
    label: "Your exercises",
  },
];

const ExercisesPage: NextPage<ExercisesPageProps> = ({ adminExercises }) => {
  const [currentTab, set_currentTab] = useState<Tab>(tabs[0]);

  const [isSubmitting, set_isSubmitting] = useState<boolean>(false);

  const [session, loading] = useSession();

  const { addToast } = useContext(ToastContext);
  const { createExercise, editExercise } = useContext(ExerciseFormContext);

  const handleEditExercise = (exercise: Exercise) => {
    editExercise(exercise);
  };

  const handleDeleteExercise = async (exercise: Exercise) => {
    set_isSubmitting(true);
    await exerciseService.deleteExercise(exercise);
    addToast({
      message: `${exercise.name} has been deleted successfully`,
    });
    exerciseService.refetchUserExercise();
    set_isSubmitting(false);
  };

  const { data: userExercises, error: getUserExercisesError } =
    exerciseService.getUserExercises(session?.user?.id);

  useEffect(() => {
    getUserExercisesError && console.log("requestError", getUserExercisesError);
  }, [getUserExercisesError]);

  return (
    <div>
      <Head>
        <title>Crosstrack - Exercises</title>
        <meta name="description" content="A workout tracker app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="p-4 sm:py-12 mx-auto">
        {!loading && !session && (
          <div className="mb-20">
            <HeroSection
              h1="Find out about our exercises and add your own"
              blob={
                <Blob className="absolute sm:w-10/12 md:w-9/12 md:left-52 md:-top-12" />
              }
              illustration={
                <img
                  className="relative object-contain w-4/6 sm:w-full lg:w-full lg:-ml-24 lg:mt-10"
                  src="/images/exercises/exercises-illustration.png"
                />
              }
              headline="Don't just pray for it, work for it"
              description="This list is used to create workouts, most of the exercises are already available, but if you want to add your own you can do so by clicking on the button below"
              actions={
                <button
                  onClick={() => signIn()}
                  disabled={loading}
                  className={`btn btn-primary btn-${loading ? "disabled" : ""}`}
                >
                  Login to add your own exercises
                </button>
              }
            />
          </div>
        )}

        <div className="relative">
          {!loading &&
            (session !== null ? (
              <div>
                <div className="tabs">
                  {tabs.map((tab) => (
                    <a
                      key={tab.id}
                      onClick={() => set_currentTab(tab)}
                      className={`tab tab-bordered ${
                        currentTab.id === tab.id ? "tab-active" : ""
                      }`}
                    >
                      {tab.label}
                    </a>
                  ))}
                </div>
              </div>
            ) : (
              <h1 className="text-xl font-black"> Our default exercises </h1>
            ))}
        </div>

        {/* {loading && [...Array(6)].map((x, i) => <ExerciseCard key={i} />)} */}

        <div className="my-10 ml-2">
          {currentTab.id === 2 && (
            <button
              onClick={() => createExercise()}
              className="btn btn-primary mb-10"
              style={{ flexBasis: "100%" }}
            >
              Add your own exercises
            </button>
          )}

          <ExerciseList
            exercises={currentTab.id === 1 ? adminExercises : userExercises}
            onEditExercise={(exercise) => handleEditExercise(exercise)}
            onDeleteExercise={(exercise) => handleDeleteExercise(exercise)}
            isSubmitting={isSubmitting}
          />
        </div>
      </div>
    </div>
  );
};

export default ExercisesPage;
