import "styles/globals.scss";
import type { AppProps } from "next/app";
import { Provider as SessionProvider } from "next-auth/client";
import Layout from "components/Layout/Layout";
import { ToastsProvider } from "components/Toast/ToastContext";
import { ExerciseFormProvider } from "components/Exercise/ExerciseForm/ExerciseFormContext";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <SessionProvider>
      <ToastsProvider>
        <ExerciseFormProvider>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </ExerciseFormProvider>
      </ToastsProvider>
    </SessionProvider>
  );
}
export default MyApp;
