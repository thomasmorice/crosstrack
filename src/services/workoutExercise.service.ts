import useSWR, { mutate } from "swr";
import fetcher from "lib/fetcher";
import { isEqual } from "lodash-es";

const endpoint = "/api/workoutExercise/";

export const workoutExerciseService = {
  getEmptyWorkoutExercise,
};

function refetchUserExercise() {
  mutate(endpoint);
}

function getEmptyWorkoutExercise(): WorkoutExercise {
  return {
    annotation: "",
    reps: "",
    exercise: null,
  };
}
