import useSWR, { mutate } from "swr";
import fetcher from "lib/fetcher";

const endpoint = "/api/exercises/";

export const exerciseService = {
  getEmptyExercise,
  getUserExercises,
  addExercise,
  editExercise,
  deleteExercise,
  searchExercises,
  refetchUserExercise,
};

function refetchUserExercise() {
  mutate(endpoint);
}

function getEmptyExercise(): Exercise {
  return {
    id: undefined,
    name: "",
    modalityId: null,
    equipmentId: "unknown",
    hasRepMax: false,
    protected: false,
  };
}

function getUserExercises(userId: string | undefined) {
  const shouldFetch = userId;
  const { data: userExercises, error } = useSWR<Exercise[]>(
    shouldFetch ? `${endpoint}` : null,
    fetcher
  );
  return {
    data: userExercises,
    error: error,
  };
}

async function addExercise(exercise: Exercise) {
  return await fetch(`/api/exercises/add`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(exercise),
  }).then((response) => {
    return response.json();
  });
}

async function editExercise(exercise: Exercise) {
  return await fetch(`/api/exercises/edit/`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(exercise),
  }).then((response) => {
    return response.json();
  });
}

async function deleteExercise(exercise: Exercise) {
  return await fetch(`/api/exercises/${exercise.id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
  }).then((response) => {
    return response.json();
  });
}

async function searchExercises(term: string): Promise<Exercise[]> {
  return await fetch(`${endpoint}/search?term=${term}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  }).then((response) => {
    return response.json();
  });
}
