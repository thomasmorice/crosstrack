import useSWR, { mutate } from "swr";
import fetcher from "lib/fetcher";
import { isEqual } from "lodash-es";

const endpoint = "/api/element/";

export const elementService = {
  getEmptyElement,
};

function refetchUserExercise() {
  mutate(endpoint);
}

function getEmptyElement(): WorkoutElement {
  return {
    workoutExercises: [],
    annotation: "",
  };
}
