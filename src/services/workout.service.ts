import useSWR, { mutate } from "swr";
import fetcher from "lib/fetcher";
import { isEqual } from "lodash-es";
import {
  uniqueNamesGenerator,
  Config,
  adjectives,
  colors,
  animals,
} from "unique-names-generator";
import { elementService } from "./element.service";

const endpoint = "/api/workouts/";

export const workoutService = {
  addWorkout,
  editWorkout,
  refetchUserWorkouts,
  deleteWorkout,
  getEmptyWorkout,
  areWorkoutsEquals,
  getUserWorkouts,
};

function areWorkoutsEquals(
  initialWorkout: Workout | undefined,
  updatedWorkout: Workout
) {
  // If initialWorkout is undefined, the workout does not exists in the DB
  // console.log("initialWorkout", initialWorkout);
  // console.log("updatedWorkout", updatedWorkout);
  return isEqual(
    initialWorkout,
    updatedWorkout
    // omit(initialWorkout ? initialWorkout : getEmptyWorkout(), [
    //   "isBeingEdited",
    //   "isDirty",
    //   "isExpanded",
    // ]),
    // omit(updatedWorkout, ["isBeingEdited", "isDirty", "isExpanded"])
  );
}

function refetchUserWorkouts() {
  mutate(endpoint);
}

function getEmptyWorkout(): Workout {
  const name: string = uniqueNamesGenerator({
    separator: "-",
    dictionaries: [adjectives, colors, animals],
    length: 2,
  }); // red_big_donkey
  return {
    timecap: null,
    name,
    type: "",
    workoutElements: [elementService.getEmptyElement()],
    description: "",
  };
}

function getUserWorkouts(userId: string | undefined) {
  const shouldFetch = userId;
  const { data: userWorkouts, error } = useSWR<Workout[]>(
    shouldFetch ? `${endpoint}` : null,
    fetcher
  );
  return {
    data: userWorkouts,
    error: error,
  };
}

async function addWorkout(workout: Workout) {
  console.log("add workout", workout);
  return await fetch(`/api/workouts/add/`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(workout),
  }).then(async (response) => {
    // console.log("response", await response.json());
    return response.json();
  });
}

async function editWorkout(workout: Workout) {
  console.log("edit workout", workout);
  return await fetch(`/api/workouts/edit/`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(workout),
  }).then(async (response) => {
    // console.log("response", await response.json());
    return response.json();
  });
}

async function deleteWorkout(workout: Workout) {
  return await fetch(`/api/workouts/delete/${workout.id}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(workout),
  }).then(async (response) => {
    return response.json();
  });
}
